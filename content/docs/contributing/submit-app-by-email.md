+++
title = "Submitting apps by email"
description = "Which information should you supply?"
date = 2022-04-07T19:30:00+02:00
updated = 2024-02-11
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Submitting apps by email!"
toc = true
top = false
+++

### Basic mail text

<mark>Please note: You can also use [LPA Helper](https://linuxphoneapps.org/lpa_helper.html) to generate a listing and send that in via email :-)</mark>

After clicking the <a href="mailto:~linuxphoneapps/linuxphoneapps.org-discuss@lists.sr.ht?subject=Please%20add%20%5BApp%20Name%5D&body=Hi%2C%0A%0Aplease%20add%20the%20app%20%5BAppName%5D.%0A%0AThe%20source%20code%20is%20being%20hosted%20at%20%5BRepository%20URL%5D.%0A%0AI%20have%20tested%20it%20on%20my%20%5BLinux%20Phone%20model%5D%20and%20would%20give%20it%20a%20rating%20of%20%5Brating%5D.%0A%0AMore%20information%20(optional)%3A%0A%0A%0ABest%20regards%2C%0A%0A%0A">link</a>, a new message window of your systems default email application should open. Please make sure to remove all the [placeholders] (everything in sqare brackets), and fill them with the matching info.

Please also include your _name_ in the form you want to be cited on the list.

### Further info

The following info would also be appreciated:

* a short **summary** of not more than a characters, summarizing the app (if it's from another source, please make sure to include the link to that source),
* a **description** (longer but concise, credit sources here too),
* the **license(s)**,
* link to the projects **website** (if it has one)
* a link to the projects **bugtracker** (if it is not gitlab/github/gitea issues)
* a link to the projects **donations page** (if any)
* a link to the **translation service** the project uses (if any),
* links with more **information** (e.g. blog or forum posts about the app),
* link to **appstream metainfo** (often named projectname.appdata.xml, project.name.metainfo.xml)

None of this is necessary, so if you are not sure what's meant or just don't have the time, just send your mail! 📮

### Screenshots

Feel free to attach screenshots of the app running on your Linux Phone. By submitting these screenshots you grant your permission to use them on LinuxPhoneApps.org and agree to licensing them under CC-BY-SA 4.0.
