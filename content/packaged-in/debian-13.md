+++
title = "Debian 12"
date = 2021-08-15T08:50:45+00:00
draft = false
+++
Debian 13 "Trixie" is Debian's current testing branch and is going to be the next stable release. There's no schedule for releasing it as a new stable release yet, but going by past releases, we can expect it some around mid-2025..

You can try to include some packages from [Debian Unstable/sid](../debian-unstable/) or even [Debian experimental](../debian-experimental/), as explained on the [Mobian Wiki](https://wiki.debian.org/Mobian/Tweaks#Add_sid_repository).

Please note: [Mobian](https://mobian.org) also have an additional repository with custom packages, which is not and will not be indexed on [repology.org](https://repology.org) (see [github issue](https://github.com/repology/repology-updater/issues/1291)), so not every package available to you on Mobian can be listed here.
