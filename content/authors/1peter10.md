+++
title = "1peter10"
description = "Creator of LinuxPhoneApps.org."
date = 2021-04-01T08:50:45+00:00
updated = 2024-03-09T08:50:45+00:00
draft = false
+++

Creator of **LinuxPhoneApps**.

You can find a little bit of "about me" on [linmob.net](https://linmob.net/authors/peter/).

You can find my poor attempts at contributing to FOSS on [github](https://github.com/1peter10), [gitlab](https://gitlab.com/1peter10), [codeberg](https://codeberg.org/1peter10) and [framagit](https://framagit.org/1peter10).

