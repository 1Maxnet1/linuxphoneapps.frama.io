+++
title = "Phosh Look"
description = "Phosh look manages ~/.config/gtk-3.0 with preset themes"
aliases = []
date = 2021-06-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "david.hamner",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "inactive", "pre-release", "early",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://source.puri.sm/david.hamner/phosh_look"
homepage = ""
bugtracker = "https://source.puri.sm/david.hamner/phosh_look/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/david.hamner/phosh_look"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "script"

+++

### Notice
Hardcoded paths limit this app to PureOS or similar systems with a user named purism right now.
