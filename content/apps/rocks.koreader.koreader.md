+++
title = "KOReader"
description = "A document viewer for DjVu, PDF, EPUB and more"
aliases = []
date = 2023-11-21
updated = 2024-01-02

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KOReader Community",]
categories = [ "document viewer", "pdf viewer", "feed reader",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "SDL",]
backends = [ "mupdf",]
services = [ "RSS",]
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Literature", "Viewer",]
programming_languages = [ "Lua",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/koreader/koreader"
homepage = "https://koreader.rocks/"
bugtracker = "https://github.com/koreader/koreader/issues"
donations = ""
translations = "https://github.com/koreader/koreader-translations"
more_information = [ "https://github.com/koreader/koreader/wiki",]
summary_source_url = "https://raw.githubusercontent.com/koreader/koreader/master/platform/appimage/koreader.appdata.xml"
screenshots = [ "https://github.com/koreader/koreader-artwork/raw/master/koreader-dictionary.png", "https://github.com/koreader/koreader-artwork/raw/master/koreader-footnotes.png", "https://github.com/koreader/koreader-artwork/raw/master/koreader-menu.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "rocks.koreader.KOReader"
scale_to_fit = ""
flathub = "https://flathub.org/apps/rocks.koreader.KOReader"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "koreader",]
appstream_xml_url = "https://raw.githubusercontent.com/koreader/koreader/master/platform/appimage/koreader.appdata.xml"
reported_by = "colinsane"
updated_by = "script"

+++


### Description

KOReader is a document viewer for E Ink devices. It supports PDF, DjVu, XPS, CBT, CBZ, FB2,
 PDB, TXT, HTML, RTF, CHM, EPUB, DOC, MOBI, and ZIP files.

[Source](https://raw.githubusercontent.com/koreader/koreader/master/platform/appimage/koreader.appdata.xml)
