+++
title = "Dissent"
description = "Lightweight and modern Discord client"
aliases = [ "apps/noappid.diamondburned.gtkcord4/", "apps/xyz.diamondb.gtkcord4/", "apps/so.libdb.gtkcord4/",]
date = 2022-04-11
updated = 2024-03-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "diamondburned",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Discord",]
packaged_in = [ "flathub", "nix_stable_23_05", "nix_stable_23_11",]
freedesktop_categories = [ "GTK", "Network", "Chat",]
programming_languages = [ "Go",]
build_systems = [ "go",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/diamondburned/gtkcord4"
homepage = "https://github.com/diamondburned/dissent"
bugtracker = "https://github.com/diamondburned/dissent/issues"
donations = "https://github.com/sponsors/diamondburned"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/diamondburned/dissent/main/so.libdb.dissent.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/diamondburned/dissent/main/.github/screenshots/01.png", "https://raw.githubusercontent.com/diamondburned/dissent/main/.github/screenshots/02.png", "https://raw.githubusercontent.com/diamondburned/dissent/main/.github/screenshots/03.png", "https://raw.githubusercontent.com/diamondburned/dissent/main/.github/screenshots/04.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/diamondburned/dissent/main/internal/icons/hicolor/scalable/apps/so.libdb.dissent.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "so.libdb.dissent"
scale_to_fit = ""
flathub = "https://flathub.org/de/apps/so.libdb.dissent"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gtkcord4",]
appstream_xml_url = "https://raw.githubusercontent.com/diamondburned/dissent/main/so.libdb.dissent.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"
feed_entry_id = "https://linuxphoneapps.org/apps/so.libdb.gtkcord4/"

+++

### Description

Dissent (formerly gtkcord4) is a third-party Discord client designed for
 a smooth, native experience on Linux desktops. Built with the GTK4
 toolkit for a modern look and feel, it delivers your favorite Discord
 app in a lightweight and visually appealing package.

[Source](https://raw.githubusercontent.com/diamondburned/dissent/main/so.libdb.dissent.metainfo.xml)

### Notice

Warning: Use at your own risk. Using a third party Discord client may result in your account being banned.

gtkcord4 uses libadwaita since v0.0.10. [Check the gtkcord3 page](https://github.com/diamondburned/gtkcord3#logging-in) for information on how to obtain a Token to log in.

Pre-built Binaries are available on gtkcord4's CI which automatically builds each release for Linux x86_64 and aarch64.
See the [Releases](https://github.com/diamondburned/gtkcord4/releases) page for the binaries.