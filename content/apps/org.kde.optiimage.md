+++
title = "OptiImage"
description = "Image optimizer to reduce the size of images"
aliases = []
date = 2021-04-29
updated = 2024-03-31

[taxonomies]
project_licenses = [ "LGPL-2.1-only", "LGPL-3.0-only", "LicenseRef-KDE-Accepted-LGPL",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Carl Schwan",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early", "inactive",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Graphics",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/carlschwan/optiimage"
homepage = "https://invent.kde.org/carlschwan/optiimage"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=OptitImage"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/carlschwan/optiimage/-/raw/master/org.kde.optiimage.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/optiimage/optiimage.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.optiimage"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/carlschwan/optiimage/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/graphics/optiimage/-/raw/master/org.kde.optiimage.metainfo.xml"
reported_by = "1peter10"
updated_by = "script"

+++


### Description

Optimize your images with OptiImage, a useful image compressor that supports
 PNG, JPEG, WebP and SVG file types.


It supports both lossless and lossy compression modes with an option whether
 to keep or not metadata of images. It additionally has a safe mode, where a new
 image is created instead of overwriting the old one.


It uses the following tools:


* oxipng for PNG images
* jpegoptim for JPEG images
* scour for SVG images
* cwebp for WebP images

[Source](https://invent.kde.org/graphics/optiimage/-/raw/master/org.kde.optiimage.metainfo.xml)