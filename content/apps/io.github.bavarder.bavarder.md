+++
title = "Bavarder"
description = "Chit-chat with an AI"
aliases = []
date = 2023-05-04
updated = 2024-03-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "0xMRTT",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "ChatGPT",]
packaged_in = []
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Bavarder/Bavarder/tree/main/data"
homepage = "https://codeberg.org/Bavarder/Bavarder"
bugtracker = "https://codeberg.org/Bavarder/Bavarder/issues"
donations = ""
translations = "https://translate.codeberg.org/engage/bavarder/"
more_information = []
summary_source_url = ""
screenshots = [ "https://codeberg.org/Bavarder/Bavarder/raw/branch/main/data/screenshots/preferences.png", "https://codeberg.org/Bavarder/Bavarder/raw/branch/main/data/screenshots/preview.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.bavarder.bavarder/1.png", "https://img.linuxphoneapps.org/io.github.bavarder.bavarder/2.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.Bavarder.Bavarder"
scale_to_fit = ""
flathub = "https://flathub.org/de/apps/io.github.Bavarder.Bavarder"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/Bavarder/Bavarder/main/data/io.github.Bavarder.Bavarder.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "script"

+++


### Description

Chit-chat with an AI

[Source](https://raw.githubusercontent.com/Bavarder/Bavarder/main/data/io.github.Bavarder.Bavarder.appdata.xml.in.in)
