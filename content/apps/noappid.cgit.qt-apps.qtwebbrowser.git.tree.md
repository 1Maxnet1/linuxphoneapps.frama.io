+++
title = "QtWebBrowser"
description = "Qt WebEngine based browser for embedded touch devices."
aliases = []
date = 2019-02-16
updated = 2023-10-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "qtwebbrowser.git",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Network", "WebBrowser",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]
requires_internet = []
tags = []

[extra]
repository = "https://code.qt.io/cgit/qt-apps/qtwebbrowser.git/tree"
homepage = "https://doc.qt.io/QtWebBrowser/"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://code.qt.io/cgit/qt-apps/qtwebbrowser.git/tree/"
screenshots = [ "https://doc.qt.io/QtWebBrowser/",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "qtwebbrowser", "qtwebbrowser-qpi",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1peter10"

+++
