+++
title = "Girens for Plex"
description = "Watch your Plex content"
aliases = []
date = 2019-09-30
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gerben Droogers",]
categories = [ "media",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Plex",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "AudioVideo", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/tijder/girens"
homepage = "https://gitlab.gnome.org/tijder/girens"
bugtracker = "https://gitlab.gnome.org/tijder/girens/issues"
donations = ""
translations = "https://hosted.weblate.org/projects/girens/girens/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/nl.g4d.Girens.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/screenshots/Albumscreen.jpg", "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/screenshots/Homescreen.jpg", "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/screenshots/Showscreen.jpg",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "nl.g4d.Girens"
scale_to_fit = ""
flathub = "https://flathub.org/apps/nl.g4d.Girens"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "girens",]
appstream_xml_url = "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/nl.g4d.Girens.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Girens is a Plex media player client. You can watch and listen to your music, shows and movies with this GTK app.

[Source](https://gitlab.gnome.org/tijder/girens/-/raw/master/data/nl.g4d.Girens.appdata.xml.in)
