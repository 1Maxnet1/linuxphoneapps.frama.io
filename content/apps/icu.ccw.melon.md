+++
title = "Melon"
description = "Stream videos on the go from different services"
aliases = []
date = 2024-03-03
updated = 2024-03-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jakob Meier",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Invidious",]
services = [ "YouTube", "Peertube", "Nebula",]
packaged_in = []
freedesktop_categories = [ "Graphics", "Network", "Player", "Video",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires always",]
tags = []

[extra]
repository = "https://codeberg.org/comcloudway/melon"
homepage = "https://codeberg.org/comcloudway/melon"
bugtracker = "https://codeberg.org/comcloudway/melon/issues"
donations = ""
translations = "https://translate.codeberg.org/engage/melon/"
more_information = [ "https://social.ccw.icu/notice/Afv5mKTEPSlS5Oeax6", "https://social.ccw.icu/notice/AfNY5RnP8jAJLxAxRA", "https://codeberg.org/comcloudway/melon/src/branch/main/screenshots/README.md",]
summary_source_url = "https://codeberg.org/comcloudway/melon/raw/branch/main/data/icu.ccw.Melon.metainfo.xml.in.in"
screenshots = [ "https://codeberg.org/comcloudway/melon/media/branch/main/screenshots/empty_feed.png", "https://codeberg.org/comcloudway/melon/media/branch/main/screenshots/server_list.png", "https://codeberg.org/comcloudway/melon/media/branch/main/screenshots/video.png",]
screenshots_img = []
svg_icon_url = "https://codeberg.org/comcloudway/melon/raw/branch/main/data/icu.ccw.Melon.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "icu.ccw.Melon"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://codeberg.org/comcloudway/melon/raw/branch/main/flatpak/icu.ccw.Melon.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://codeberg.org/comcloudway/melon/raw/branch/main/data/icu.ccw.Melon.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "script"

+++


### Description

Melon allows you to watch videos, subscribe to channels and bookmark playlists from multiple services.


Access to these services is provided via so called servers (aka plugins), which communicate with the API of said service.
 The following services are currently supported:


* Invidious (to access YouTube)
* PeerTube (experimental)
* Nebula (account required to watch videos)

[Source](https://codeberg.org/comcloudway/melon/raw/branch/main/data/icu.ccw.Melon.metainfo.xml.in.in)