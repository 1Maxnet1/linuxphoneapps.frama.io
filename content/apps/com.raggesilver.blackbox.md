+++
title = "Black Box"
description = "A beautiful GTK 4 terminal"
aliases = []
date = 2022-06-30
updated = 2024-03-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Paulo Queiroz",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/raggesilver/blackbox"
homepage = "https://gitlab.gnome.org/raggesilver/blackbox"
bugtracker = "https://gitlab.gnome.org/raggesilver/blackbox/issues"
donations = "https://www.patreon.com/raggesilver"
translations = "https://gitlab.gnome.org/raggesilver/blackbox/issues"
more_information = []
summary_source_url = "https://gitlab.gnome.org/raggesilver/blackbox/-/raw/main/data/com.raggesilver.BlackBox.metainfo.xml.in"
screenshots = [ "https://i.imgur.com/2zH4FIj.png", "https://i.imgur.com/38c2eX4.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.raggesilver.BlackBox"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.raggesilver.BlackBox"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "blackbox-terminal",]
appstream_xml_url = "https://gitlab.gnome.org/raggesilver/blackbox/-/raw/main/data/com.raggesilver.BlackBox.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "script"

+++


### Description

Black Box is a native terminal emulator for GNOME that offers superb
 theming options.


With Black Box you can:


* Set colors schemes and integrate them with the rest of the window
* Customize font and size
* Customize keyboard shortcuts
* Render Sixel escape sequences
* Fully hide the window headerbar
* Quickly open links and files by ctrl+clicking file paths and URLs
* Easily paste file paths by dragging them into the window


This app is written in Vala and uses GTK 4, libadwaita, and VTE.

[Source](https://gitlab.gnome.org/raggesilver/blackbox/-/raw/main/data/com.raggesilver.BlackBox.metainfo.xml.in)