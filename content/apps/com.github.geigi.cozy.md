+++
title = "Cozy"
description = "Listen to audio books"
aliases = []
date = 2020-10-15
updated = 2024-03-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Julian Geywitz",]
categories = [ "audiobook player",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gnuguix", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Audio", "AudioVideo", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/geigi/cozy/"
homepage = "https://cozy.sh"
bugtracker = "https://github.com/geigi/cozy/issues"
donations = "https://www.patreon.com/geigi"
translations = "https://www.transifex.com/geigi/cozy/"
more_information = [ "https://thisweek.gnome.org/posts/2024/03/twig-138/#third-party-projects",]
summary_source_url = "https://raw.githubusercontent.com/geigi/cozy/master/data/com.github.geigi.cozy.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/geigi/cozy/img/img/screenshot1.png", "https://raw.githubusercontent.com/geigi/cozy/img/img/screenshot2.png", "https://raw.githubusercontent.com/geigi/cozy/img/img/screenshot3.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.geigi.cozy"
scale_to_fit = "com.github.geigi.cozy"
flathub = "https://flathub.org/apps/com.github.geigi.cozy"
flatpak_link = "https://flathub.org/apps/com.github.geigi.cozy.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/geigi/cozy/master/com.github.geigi.cozy.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "cozy-audiobooks",]
appstream_xml_url = "https://raw.githubusercontent.com/geigi/cozy/master/data/com.github.geigi.cozy.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"

+++

### Description

Do you like audio books? Then lets get cozy!


Cozy is a audio book player. Here are some of the features:


* Import all your audio books into Cozy to browse them comfortably
* Listen to your DRM free mp3, m4b, m4a (aac, ALAC, …), flac, ogg and wav audio books
* Remembers your playback position
* Sleep timer
* Playback speed control for each book individually
* Search your library
* Multiple storage location support
* Offline Mode! This allows you to keep an audio book on your internal storage if you store your audio books on an external or network drive. Perfect to listen to on the go!
* Drag and Drop to import new audio books
* Sort your audio books by author, reader and name

[Source](https://raw.githubusercontent.com/geigi/cozy/master/data/com.github.geigi.cozy.appdata.xml)

### Notice

Used GTK3/libhandy before release 1.3.
The app works great out of the box since release 1.2.0.