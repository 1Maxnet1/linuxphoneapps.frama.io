+++
title = "Weather Mobile"
description = "A simple GTK weather app to play with Linux Mobile development in GTK4 and OpenWeather API."
aliases = []
date = 2021-02-06
updated = 2023-09-30

[taxonomies]
project_licenses = [ "not specified",]
metadata_licenses = []
app_author = [ "tiggilyboo",]
categories = [ "weather",]
mobile_compatibility = [ "5",]
status = [ "early", "pre-release", "inactive",]
frameworks = [ "GTK4",]
backends = [ "OpenWeather API",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Tiggilyboo/weather-mobile"
homepage = ""
bugtracker = "https://github.com/Tiggilyboo/weather-mobile/issues/"
donations = ""
translations = ""
more_information = [ "https://fosstodon.org/@linmob/105703922646357593",]
summary_source_url = "https://github.com/Tiggilyboo/weather-mobile"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "1peter10"

+++

### Notice
WIP, lacks desktop file and PKGBUILD does not work.
