+++
title = "Phonic"
description = "Phonic is a GTK audiobook player targeted at mobile Linux."
aliases = []
date = 2020-09-06
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "hamner",]
categories = [ "audiobook player",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = []

[extra]
repository = "https://sr.ht/~hamner/Phonic/"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~hamner/Phonic"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "phonic",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "script"

+++
