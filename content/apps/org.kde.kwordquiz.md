+++
title = "KWordQuiz"
description = "Flash card trainer"
aliases = []
date = 2023-12-28
updated = 2024-03-31

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = []
categories = [ "education",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_unstable", "opensuse_tumbleweed", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "Education", "Languages",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/education/kwordquiz"
homepage = "https://apps.kde.org/kwordquiz"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=kwordquiz"
donations = "https://www.kde.org/community/donations/?app=kwordquiz&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/education/kwordquiz/-/raw/master/org.kde.kwordquiz.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kwordquiz/editor.png", "https://cdn.kde.org/screenshots/kwordquiz/flashcard.png", "https://cdn.kde.org/screenshots/kwordquiz/home.png", "https://cdn.kde.org/screenshots/kwordquiz/multiple-choice.png", "https://cdn.kde.org/screenshots/kwordquiz/question-response.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kwordquiz"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kwordquiz"
flatpak_link = "https://flathub.org/apps/org.kde.kwordquiz.flatpakref"
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/kwordquiz"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kwordquiz",]
appstream_xml_url = "https://invent.kde.org/education/kwordquiz/-/raw/master/org.kde.kwordquiz.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"

+++

### Description

KWordQuiz is a general purpose flash card program. It can be used for vocabulary learning and many other subjects. It provides an editor and five different flashcard quiz modes. It uses the KVTML file format and contributed files can be downloaded from within the application.

[Source](https://invent.kde.org/education/kwordquiz/-/raw/master/org.kde.kwordquiz.appdata.xml)