+++
title = "GoForIt!"
description = "A stylish to-do list with built-in productivity timer"
aliases = []
date = 2024-03-03

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GoForIt! developers",]
categories = [ "productivity", "task management",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "GTK3", "granite",]
backends = []
services = [ "todo.txt",]
packaged_in = [ "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Office", "ProjectManagement", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/JMoerman/Go-For-It"
homepage = "https://jmoerman.github.io/go-for-it/"
bugtracker = "https://github.com/JMoerman/Go-For-It/issues"
donations = "https://jmoerman.github.io/donate/"
translations = "https://hosted.weblate.org/projects/go-for-it/"
more_information = []
summary_source_url = "https://flathub.org/apps/de.manuel_kehl.go-for-it"
screenshots = [ "https://raw.githubusercontent.com/JMoerman/Go-For-It/master/screenshot.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/JMoerman/Go-For-It/master/data/icons/128x128/apps/go-for-it.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "de.manuel_kehl.go-for-it"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.manuel_kehl.go-for-it"
flatpak_link = "https://flathub.org/apps/de.manuel_kehl.go-for-it.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/de.manuel_kehl.go-for-it/master/de.manuel_kehl.go-for-it.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "go-for-it",]
appstream_xml_url = "https://raw.githubusercontent.com/JMoerman/Go-For-It/master/data/go-for-it.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = ""

+++

### Description

GoForIt! is a simple and stylish productivity app, featuring a to-do list, merged with a timer that keeps your focus on the current task.


To-do lists are stored in the Todo.txt format. This simplifies synchronization with mobile devices and makes it possible to edit tasks using other front-ends.

[Source](https://raw.githubusercontent.com/JMoerman/Go-For-It/master/data/go-for-it.appdata.xml.in.in)

### Notice

Some dialogues (create new list, settings) do not fully fit the screen, but can be operated mostly fine.
