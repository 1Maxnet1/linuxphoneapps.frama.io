+++
title = "Door Knocker"
description = "Check the availability of portals"
aliases = []
date = 2024-02-11
updated = 2024-03-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "tytan652",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/tytan652/door-knocker"
homepage = "https://codeberg.org/tytan652/door-knocker"
bugtracker = "https://codeberg.org/tytan652/door-knocker/issues"
donations = "https://linksta.cc/@tytan652"
translations = "https://translate.codeberg.org/engage/door-knocker/"
more_information = []
summary_source_url = "https://flathub.org/apps/xyz.tytanium.DoorKnocker"
screenshots = [ "https://codeberg.org/tytan652/door-knocker/raw/commit/e2adc28dff8a54850a5c946b947dfcea058ff38c/data/screenshots/door-knocker_dark.png", "https://codeberg.org/tytan652/door-knocker/raw/commit/e2adc28dff8a54850a5c946b947dfcea058ff38c/data/screenshots/door-knocker_light.png", "https://codeberg.org/tytan652/door-knocker/raw/commit/e2adc28dff8a54850a5c946b947dfcea058ff38c/data/screenshots/door-knocker_screen_cast_dialog_dark.png", "https://codeberg.org/tytan652/door-knocker/raw/commit/e2adc28dff8a54850a5c946b947dfcea058ff38c/data/screenshots/door-knocker_screen_cast_dialog_light.png",]
screenshots_img = []
svg_icon_url = "https://codeberg.org/tytan652/door-knocker/src/commit/e24bf82c19efd963b0cf55c7abec18a5dfe6a8e7/data/icons/hicolor/scalable/apps/xyz.tytanium.DoorKnocker.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "xyz.tytanium.DoorKnocker"
scale_to_fit = ""
flathub = "https://flathub.org/apps/xyz.tytanium.DoorKnocker"
flatpak_link = "https://dl.flathub.org/repo/appstream/xyz.tytanium.DoorKnocker.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "door-knocker",]
appstream_xml_url = "https://codeberg.org/tytan652/door-knocker/raw/branch/main/data/xyz.tytanium.DoorKnocker.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "script"

+++


### Description

Door Knocker allows you to check availability of all portals provided by xdg-desktop-portal.

[Source](https://codeberg.org/tytan652/door-knocker/raw/branch/main/data/xyz.tytanium.DoorKnocker.appdata.xml.in.in)
