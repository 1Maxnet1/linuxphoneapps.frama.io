+++
title = "Electrolysis"
description = "EV Charging interface for Linux"
aliases = []
date = 2024-02-03
updated = 2024-03-31

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "undef",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = [ "early", "pre-release",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Chargefox", "Tesla Supercharger",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://gitlab.com/undef1/electrolysis"
homepage = "https://gitlab.com/undef1/electrolysis"
bugtracker = "https://gitlab.com/undef1/electrolysis/-/issues"
donations = ""
translations = ""
more_information = [ "https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss/%3Cf13677b1-a592-457e-903f-b2a22048666c%40undef.tools%3E",]
summary_source_url = "https://gitlab.com/undef1/electrolysis/-/raw/master/doc/com.gitlab.undef1.Electrolysis.metainfo.xml"
screenshots = [ "https://gitlab.com/undef1/electrolysis/-/raw/master/doc/electrolysis-charge-session.png?ref_type=heads&inline=false", "https://gitlab.com/undef1/electrolysis/-/raw/master/doc/electrolysis-main-screen.png?ref_type=heads&inline=false", "https://gitlab.com/undef1/electrolysis/-/raw/master/doc/electrolysis-main-screen.png?ref_type=heads&inline=false",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/undef1/electrolysis/-/raw/master/resources/Electrolysis.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "com.gitlab.undef1.Electrolysis"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/undef1/electrolysis/-/raw/master/doc/com.gitlab.undef1.Electrolysis.metainfo.xml"
reported_by = "Undef"
updated_by = "script"

+++


### Description

Sadly public charging of electric vehicles often requires either an app or at least an account. Until now, those apps only existed for Android and iOS. It's time we changed that and created something for Mobile Linux.


Electrolysis provides a native GTK application for EV charging.

[Source](https://gitlab.com/undef1/electrolysis/-/raw/master/doc/com.gitlab.undef1.Electrolysis.metainfo.xml)