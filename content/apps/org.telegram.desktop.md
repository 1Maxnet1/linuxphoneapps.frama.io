+++
title = "Telegram Desktop"
description = "Fast. Secure. Powerful."
aliases = []
date = 2019-02-16
updated = 2024-01-02

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "John Preston",]
categories = [ "chat",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "QtWidgets",]
backends = []
services = [ "Telegram",]
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "InstantMessaging", "Network",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/telegramdesktop/tdesktop"
homepage = "https://desktop.telegram.org/"
bugtracker = "https://github.com/telegramdesktop/tdesktop/issues"
donations = ""
translations = "https://translations.telegram.org/"
more_information = [ "https://wiki.postmarketos.org/wiki/Telegram_Desktop", "https://web.archive.org/web/20230921180008/https://wiki.mobian.org/doku.php?id=telegram-desktop",]
summary_source_url = "https://raw.githubusercontent.com/telegramdesktop/tdesktop/dev/lib/xdg/org.telegram.desktop.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/telegramdesktop/tdesktop/dev/docs/assets/preview.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.telegram.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.telegram.desktop"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/flathub/org.telegram.desktop/master/org.telegram.desktop.yml"
snapcraft = ""
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/telegramdesktop/tdesktop/master/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "telegram-desktop",]
appstream_xml_url = "https://raw.githubusercontent.com/telegramdesktop/tdesktop/dev/lib/xdg/org.telegram.desktop.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Pure instant messaging — simple, fast, secure, and synced across all your devices. One of the world's top 10 most downloaded apps with over 500 million active users.


FAST: Telegram is the fastest messaging app on the market, connecting people via a unique, distributed network of data centers around the globe.


SYNCED: You can access your messages from all your phones, tablets and computers at once. Telegram apps are standalone, so you don’t need to keep your phone connected. Start typing on one device and finish the message from another. Never lose your data again.


UNLIMITED: You can send media and files, without any limits on their type and size. Your entire chat history will require no disk space on your device, and will be securely stored in the Telegram cloud for as long as you need it.


SECURE: We made it our mission to provide the best security combined with ease of use. Everything on Telegram, including chats, groups, media, etc. is encrypted using a combination of 256-bit symmetric AES encryption, 2048-bit RSA encryption, and Diffie–Hellman secure key exchange.


100% FREE & OPEN: Telegram has a fully documented and free API for developers, open source apps and verifiable builds to prove the app you download is built from the exact same source code that is published.


POWERFUL: You can create group chats with up to 200,000 members, share large videos, documents of any type (.DOCX, .MP3, .ZIP, etc.) up to 2 GB each, and even set up bots for specific tasks. Telegram is the perfect tool for hosting online communities and coordinating teamwork.


RELIABLE: Built to deliver your messages using as little data as possible, Telegram is the most reliable messaging system ever made. It works even on the weakest mobile connections.


FUN: Telegram has powerful photo and video editing tools, animated stickers and emoji, fully customizable themes to change the appearance of your app, and an open sticker/GIF platform to cater to all your expressive needs.


SIMPLE: While providing an unprecedented array of features, we take great care to keep the interface clean. Telegram is so simple you already know how to use it.


PRIVATE: We take your privacy seriously and will never give any third parties access to your data. You can delete any message you ever sent or received for both sides, at any time and without a trace. Telegram will never use your data to show you ads.


We keep expanding the boundaries of what you can do with a messaging app. Don’t wait years for older messengers to catch up with Telegram — join the revolution today.

[Source](https://raw.githubusercontent.com/telegramdesktop/tdesktop/dev/lib/xdg/org.telegram.desktop.metainfo.xml)

### Notice

Needs tweaking to work well on mobile, see links under "More information" for tips.
