+++
title = "Valuta"
description = "Convert between currencies"
aliases = []
date = 2024-02-12
updated = 2024-03-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ideve Core",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Google Finance Currency Conversion",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires always",]
tags = []

[extra]
repository = "https://github.com/ideveCore/valuta"
homepage = "https://github.com/ideveCore/valuta"
bugtracker = "https://github.com/ideveCore/valuta/issues"
donations = "https://ko-fi.com/idevecore"
translations = "https://hosted.weblate.org/engage/currency-converter/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/ideveCore/Valuta/main/data/io.github.idevecore.Valuta.appdata.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/ideveCore/valuta/developer/data/screenshots/01.png", "https://raw.githubusercontent.com/ideveCore/valuta/developer/data/screenshots/02.png", "https://raw.githubusercontent.com/ideveCore/valuta/developer/data/screenshots/03.png", "https://raw.githubusercontent.com/ideveCore/valuta/developer/data/screenshots/04.png",]
screenshots_img = []
svg_icon_url = "https://github.com/ideveCore/Valuta/blob/main/data/icons/hicolor/scalable/apps/io.github.idevecore.Valuta.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "io.github.idevecore.CurrencyConverter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.idevecore.CurrencyConverter"
flatpak_link = "https://flathub.org/apps/io.github.idevecore.CurrencyConverter.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/ideveCore/Valuta/main/io.github.idevecore.Valuta.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/ideveCore/Valuta/main/data/io.github.idevecore.Valuta.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "script"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.idevecore.currencyconverter/"

+++


### Description

Valuta is a simple and fast conversion tool, ideal for those who need to convert between different currencies repeatedly. Use it while traveling, budgeting, or anytime else you need to quickly convert between two currencies.

[Source](https://raw.githubusercontent.com/ideveCore/Valuta/main/data/io.github.idevecore.Valuta.appdata.xml.in.in)