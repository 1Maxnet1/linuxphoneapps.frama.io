+++
title = "Decibels"
description = "Play audio files"
aliases = [ "apps/com.vixalien.decibels/",]
date = 2023-11-25
updated = 2024-03-31

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Angelo Verlain",]
categories = [ "audio player", "music player",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "GStreamer",]
services = []
packaged_in = [ "aur", "flathub", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Player",]
programming_languages = [ "TypeScript",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/vixalien/decibels"
homepage = "https://gitlab.gnome.org/vixalien/decibels"
bugtracker = "https://gitlab.gnome.org/vixalien/decibels/-/issues"
donations = "https://www.buymeacoffee.com/vixalien"
translations = "https://gitlab.gnome.org/vixalien/decibels/-/tree/main/po?ref_type=heads"
more_information = [ "https://apps.gnome.org/Decibels/",]
summary_source_url = "https://raw.githubusercontent.com/vixalien/decibels/main/data/com.vixalien.decibels.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/vixalien/decibels/-/raw/main/data/screenshots/screenshot-1.png?ref_type=heads", "https://gitlab.gnome.org/vixalien/decibels/-/raw/main/data/screenshots/screenshot-2.png?ref_type=heads",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/Incubator/decibels/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.Decibels.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Decibels"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.vixalien.decibels"
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/GNOME/Incubator/decibels/-/raw/main/build-aux/flatpak/org.gnome.Decibels.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "decibels",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/Incubator/decibels/-/raw/main/data/org.gnome.Decibels.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "script"
feed_entry_id = "https://linuxphoneapps.org/apps/com.vixalien.decibels/"

+++


### Description

Current features:


* Shows the waveform of the track
* Adjust playback speed
* Easy seek controls

[Source](https://raw.githubusercontent.com/vixalien/decibels/main/data/com.vixalien.decibels.metainfo.xml.in.in)