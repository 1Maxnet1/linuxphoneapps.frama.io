+++
title = "Folio"
description = "Take notes in Markdown"
aliases = []
date = 2024-03-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Greg Ross",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub", "nix_unstable", "snapcraft",]
freedesktop_categories = [ "GNOME", "GTK", "TextEditor", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/toolstack/Folio"
homepage = "https://github.com/toolstack/Folio"
bugtracker = "https://github.com/toolstack/Folio/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.toolstack.Folio"
screenshots = [ "https://raw.githubusercontent.com/toolstack/Folio/main/meta/folio-desktop-dark.png", "https://raw.githubusercontent.com/toolstack/Folio/main/meta/folio-desktop.png", "https://raw.githubusercontent.com/toolstack/Folio/main/meta/folio-mobile.png", "https://raw.githubusercontent.com/toolstack/Folio/main/meta/folio-preferences.png", "https://raw.githubusercontent.com/toolstack/Folio/main/meta/folio-three-pane-mode.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/toolstack/Folio/main/data/icons/hicolor/scalable/apps/com.toolstack.Folio.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "com.toolstack.Folio"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.toolstack.Folio"
flatpak_link = "https://flathub.org/apps/com.toolstack.Folio.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/toolstack/Folio/main/com.toolstack.Folio.json"
snapcraft = "https://snapcraft.io/folio"
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/toolstack/Folio/main/snap/snapcraft.yaml"
appimage_x86_64_url = "https://github.com/toolstack/Folio/releases"
appimage_aarch64_url = ""
repology = [ "folio",]
appstream_xml_url = "https://raw.githubusercontent.com/toolstack/Folio/main/data/app.metainfo.xml.in"
reported_by = "1peter10"
updated_by = ""

+++

### Description

Create notebooks, take notes in markdown


Features:


* Almost WYSIWYG markdown rendering
* Searchable through GNOME search
* Highlight and strikethrough text formatting
* App themeing based on notebook color
* Trash can

[Source](https://raw.githubusercontent.com/toolstack/Folio/main/data/app.metainfo.xml.in)

### Notice

Folio is a maintained fork of [Paper](https://linuxphoneapps.org/apps/io.posidon.paper/).
