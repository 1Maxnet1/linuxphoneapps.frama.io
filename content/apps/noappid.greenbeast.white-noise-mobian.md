+++
title = "White Noise Mobian"
description = "Basic white noise app for Mobian"
aliases = []
date = 2021-09-12
updated = 2022-12-16

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "greenbeast",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early", "inactive",]
frameworks = [ "GTK3", "SDL",]
backends = [ "pygame",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/greenbeast/white_noise_mobian"
homepage = ""
bugtracker = "https://gitlab.com/greenbeast/white_noise_mobian/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/greenbeast/white_noise_mobian"
screenshots = [ "https://gitlab.com/greenbeast/white_noise_mobian/-/blob/master/screenshot/white_noise_screenshot.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "greenbeast"
updated_by = "1peter10"

+++

### Notice

A .deb is provided in the repository. It works on other distributions too, install it via icon_install.py. Adding env SDL_VIDEODRIVER=wayland to the launchers Exec= line can improve things on distributions with new enough SDL (2.0.16 or newer).

Last commit 2021-09-27.
