+++
title = "Color Palette"
description = "Color Palette tool"
aliases = []
date = 2020-02-06
updated = 2023-12-31

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Zander Brown",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Development", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/design/palette"
homepage = "https://gitlab.gnome.org/World/design/palette"
bugtracker = "https://gitlab.gnome.org/World/design/palette/issues"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.design.Palette/",]
summary_source_url = "https://gitlab.gnome.org/World/design/palette/-/raw/master/data/org.gnome.design.Palette.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/World/design/palette/raw/master/data/screenshots/screenshot1.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.design.Palette"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.design.Palette"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "palette",]
appstream_xml_url = "https://gitlab.gnome.org/World/design/palette/-/raw/master/data/org.gnome.design.Palette.metainfo.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description
Tool for viewing the GNOME color palette as defined by the design guidelines.

[Source](https://gitlab.gnome.org/World/design/palette/-/raw/master/data/org.gnome.design.Palette.metainfo.xml.in)
