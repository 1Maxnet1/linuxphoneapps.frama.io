+++
title = "Hash-o-matic"
description = "Check hashes for your files"
aliases = []
date = 2021-12-18
updated = 2024-03-31

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Carl Schwan",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami", "QtQuick 6",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "not packaged yet", "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/utilities/hash-o-matic/"
homepage = "https://apps.kde.org/hashomatic"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Hash-o-Matix"
donations = "https://kde.org/community/donations/?app=neochat"
translations = ""
more_information = [ "https://carlschwan.eu/2021/12/18/more-kde-apps/", "https://plasma-mobile.org/2024/03/01/plasma-6/#hash-o-matic",]
summary_source_url = "https://invent.kde.org/utilities/hash-o-matic/-/raw/master/org.kde.hashomatic.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/hashomatic/compare.png", "https://cdn.kde.org/screenshots/hashomatic/generate.png", "https://cdn.kde.org/screenshots/hashomatic/verify.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/utilities/hash-o-matic/-/raw/master/org.kde.hashomatic.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.hashomatic"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/utilities/hash-o-matic/-/raw/master/org.kde.hashomatic.metainfo.xml"
reported_by = "1peter10"
updated_by = "script"

+++


### Description

Verifying that a file you downloaded or received is actually the one you were
 expecting is often overlooked or too time-consuming to do. At the same time, it
 has become very easy to get your hands on a file that has been tampered with, due
 to the mass increase of malicious webpages and other actors.


This tool aims to solve that. Hash-o-matic comes with a simple & clean UI, allowing
 anyone, from any age and experience group, to generate, compare and verify MD5,
 SHA-256 and SHA-1 hashes.


Hash-o-Matic also support verifying a file with the help of a PGP signature.

[Source](https://invent.kde.org/utilities/hash-o-matic/-/raw/master/org.kde.hashomatic.metainfo.xml)