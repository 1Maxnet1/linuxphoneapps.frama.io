+++
title = "Railway"
description = "Find all your travel information"
aliases = []
date = 2022-03-22
updated = 2024-03-31

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Julian Schmidhuber",]
categories = [ "public transport",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "hafas-rs",]
services = [ "hafas",]
packaged_in = [ "alpine_edge", "aur", "debian_unstable", "devuan_unstable", "flathub", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Maps", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson", "cargo",]
requires_internet = [ "recommends always",]
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.com/schmiddi-on-mobile/railway"
homepage = "https://mobile.schmidhuberj.de/railway"
bugtracker = "https://gitlab.com/schmiddi-on-mobile/railway/-/issues"
donations = "https://gitlab.com/schmiddi-on-mobile/railway#donate"
translations = "https://hosted.weblate.org/engage/schmiddi-on-mobile/"
more_information = [ "https://apps.gnome.org/DieBahn/",]
summary_source_url = "https://gitlab.com/schmiddi-on-mobile/railway/-/raw/master/data/de.schmidhuberj.DieBahn.metainfo.xml.in"
screenshots = [ "https://gitlab.com/schmiddi-on-mobile/railway/-/raw/master/data/screenshots/overview.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/schmiddi-on-mobile/railway/-/raw/master/data/icons/de.schmidhuberj.DieBahn.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "de.schmidhuberj.DieBahn"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.schmidhuberj.DieBahn"
flatpak_link = "https://flathub.org/apps/de.schmidhuberj.DieBahn.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "diebahn",]
appstream_xml_url = "https://gitlab.com/schmiddi-on-mobile/railway/-/raw/master/data/de.schmidhuberj.DieBahn.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_repology"

+++

### Description

Railway lets you look up travel information across networks and borders
 without having to navigate through different websites. Due to the adaptive
 design, it is suitable to plan your trip in advance or mobile on the go:


* View your tripsʼ details including platforms and delays
* Bookmark current and future trips as well as frequent search
* Make use of advance search options like filtering by mode of transport


And all that for networks from all around the world, but mostly from Europe.

[Source](https://gitlab.com/schmiddi-on-mobile/railway/-/raw/master/data/de.schmidhuberj.DieBahn.metainfo.xml.in)

### Notice

Renamed from DieBahn to Railway with update in September 2023 with version 2.0.0.