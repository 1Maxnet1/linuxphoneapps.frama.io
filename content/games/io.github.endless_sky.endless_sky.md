+++
title = "Endless Sky"
description = "Space exploration and combat game"
aliases = []
date = 2021-03-14
updated = 2024-03-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Michael Zahniser",]
categories = [ "game",]
mobile_compatibility = [ "1",]
status = []
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Game", "Simulation",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/endless-sky/endless-sky"
homepage = "https://endless-sky.github.io/"
bugtracker = "https://github.com/endless-sky/endless-sky/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/endless-sky/endless-sky/master/io.github.endless_sky.endless_sky.appdata.xml"
screenshots = [ "https://dl.flathub.org/media/io/github/endless_sky.endless_sky/bfc438ca2a0bc940272a54c84ad22ab5/screenshots/image-1_orig.png", "https://dl.flathub.org/media/io/github/endless_sky.endless_sky/bfc438ca2a0bc940272a54c84ad22ab5/screenshots/image-2_orig.png", "https://dl.flathub.org/media/io/github/endless_sky.endless_sky/bfc438ca2a0bc940272a54c84ad22ab5/screenshots/image-3_orig.png", "https://dl.flathub.org/media/io/github/endless_sky.endless_sky/bfc438ca2a0bc940272a54c84ad22ab5/screenshots/image-4_orig.png", "https://dl.flathub.org/media/io/github/endless_sky.endless_sky/bfc438ca2a0bc940272a54c84ad22ab5/screenshots/image-5_orig.png", "https://dl.flathub.org/media/io/github/endless_sky.endless_sky/bfc438ca2a0bc940272a54c84ad22ab5/screenshots/image-6_orig.png", "https://dl.flathub.org/media/io/github/endless_sky.endless_sky/bfc438ca2a0bc940272a54c84ad22ab5/screenshots/image-7_orig.png", "https://dl.flathub.org/media/io/github/endless_sky.endless_sky/bfc438ca2a0bc940272a54c84ad22ab5/screenshots/image-8_orig.png", "https://dl.flathub.org/media/io/github/endless_sky.endless_sky/bfc438ca2a0bc940272a54c84ad22ab5/screenshots/image-9_orig.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.endless_sky.endless_sky"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.endless_sky.endless_sky"
flatpak_link = "https://flathub.org/apps/io.github.endless_sky.endless_sky.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "endless-sky",]
appstream_xml_url = "https://raw.githubusercontent.com/endless-sky/endless-sky/master/io.github.endless_sky.endless_sky.appdata.xml"
reported_by = "-Euso-"
updated_by = "script"

+++

### Description

Explore other star systems. Earn money by trading, carrying passengers, or completing missions. Use your earnings to buy a better ship or to upgrade the weapons and engines on your current one. Blow up pirates. Take sides in a civil war. Or leave human space behind and hope to find some friendly aliens whose culture is more civilized than your own...


Endless Sky is a sandbox-style space exploration game similar to Elite, Escape Velocity, or Star Control. You start out as the captain of a tiny spaceship and can choose what to do from there. The game includes a major plot line and many minor missions, but you can choose whether you want to play through the plot or strike out on your own as a merchant or bounty hunter or explorer.

[Source](https://raw.githubusercontent.com/endless-sky/endless-sky/master/io.github.endless_sky.endless_sky.appdata.xml)