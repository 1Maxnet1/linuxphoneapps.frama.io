+++
title = "Shootin Stars"
description = "Round based endless runner style space shooter"
aliases = []
date = 2021-12-02
updated = 2024-03-09

[taxonomies]
project_licenses = ["MIT"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["5"]
status = [ "early", "inactive",]
frameworks = [ "Godot",]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = [ "Python",]
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.com/greenbeast/shootin-stars"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/greenbeast/shootin-stars"
screenshots = ["https://gitlab.com/greenbeast/shootin-stars"]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "Hank"
updated_by = "1peter10"

+++

### Notice

Built for the PinePhone and runs just fine
