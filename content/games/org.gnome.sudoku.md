+++
title = "GNOME Sudoku"
description = "Test your logic skills in this number grid puzzle"
aliases = []
date = 2023-02-07
updated = 2024-03-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = []
categories = [ "game",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Game", "LogicGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-sudoku"
homepage = "https://wiki.gnome.org/Apps/Sudoku"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = []
summary_source_url = "https://flathub.org/apps/org.gnome.Sudoku"
screenshots = [ "https://dl.flathub.org/media/org/gnome/Sudoku/5472e1be260ed21c3d4b95a8ca53320e/screenshots/image-1_orig.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Sudoku"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Sudoku"
flatpak_link = "https://flathub.org/apps/org.gnome.Sudoku.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-sudoku",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/org.gnome.Sudoku.appdata.xml.in"
reported_by = "1peter10"
updated_by = "script"

+++

### Description

Play the popular Japanese logic game. GNOME Sudoku is a must-install for
 Sudoku lovers, with a simple, unobtrusive interface that makes playing
 Sudoku fun for players of any skill level.


Each game is assigned a difficulty similar to those given by newspapers
 and websites, so your game will be as easy or as difficult as you want it
 to be.


If you like to play on paper, you can print games out. You can choose how
 many games you want to print per page and what difficulty of games you
 want to print: as a result, GNOME Sudoku can act a renewable Sudoku book
 for you.

[Source](https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/org.gnome.Sudoku.appdata.xml.in)