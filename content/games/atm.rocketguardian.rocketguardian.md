+++
title = "Rocket Guardian"
description = "Rocket Guardian is a android game about a guardian that must protect a city from the falling zombies."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = ["GPL-3.0-only"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["needs testing"]
status = []
frameworks = ["libGDX"]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/atorresm/rocket-guardian"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/atorresm/rocket-guardian"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "atm.rocketguardian.RocketGuardian"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = ""

+++
