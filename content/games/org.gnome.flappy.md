+++
title = "Flappy-gnome"
description = "Flappy GNOME is a side-scrolling game development tutorial using Vala and GTK+."
aliases = []
date = 2019-03-04
updated = 2024-03-09

[taxonomies]
project_licenses = ["not specified"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["needs testing"]
status = [ "early", "inactive",]
frameworks = ["GTK3"]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Game",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.gnome.org/robertroth/flappy-gnome-tutorial"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/robertroth/flappy-gnome-tutorial"
screenshots = ["https://gitlab.gnome.org/robertroth/flappy-gnome-tutorial"]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Flappy"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/robertroth/flappy-gnome-tutorial/-/raw/master/org.gnome.Flappy.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1peter10"

+++
