+++
title = "Shattered Pixel Dungeon"
description = "Roguelike RPG, with pixel art graphics and lots of variety and replayability"
aliases = []
date = 2021-03-14
updated = 2024-03-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Evan Debenham",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Game", "StrategyGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/00-Evan/shattered-pixel-dungeon"
homepage = "https://shatteredpixel.com/"
bugtracker = "https://github.com/00-Evan/shattered-pixel-dungeon/issues"
donations = "https://www.patreon.com/ShatteredPixel"
translations = "https://www.transifex.com/shattered-pixel/shattered-pixel-dungeon/"
more_information = []
summary_source_url = "https://flathub.org/apps/com.shatteredpixel.shatteredpixeldungeon"
screenshots = [ "https://raw.githubusercontent.com/flathub/com.shatteredpixel.shatteredpixeldungeon/master/screenshot1.png", "https://raw.githubusercontent.com/flathub/com.shatteredpixel.shatteredpixeldungeon/master/screenshot2.png", "https://raw.githubusercontent.com/flathub/com.shatteredpixel.shatteredpixeldungeon/master/screenshot3.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.shatteredpixel.shatteredpixeldungeon"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.shatteredpixel.shatteredpixeldungeon"
flatpak_link = "https://flathub.org/apps/com.shatteredpixel.shatteredpixeldungeon.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "shattered-pixel-dungeon",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/com.shatteredpixel.shatteredpixeldungeon/master/com.shatteredpixel.shatteredpixeldungeon.appdata.xml"
reported_by = "Moxvallix"
updated_by = "script"

+++

### Notice

Forked by ebolalex to allow it to run. Best in fullscreen mode. Needs java.