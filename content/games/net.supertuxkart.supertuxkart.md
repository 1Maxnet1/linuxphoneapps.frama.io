+++
title = "SuperTuxKart"
description = "A 3D open-source kart racing game"
aliases = []
date = 2019-02-01
updated = 2024-03-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "SuperTuxKart Team",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = []
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "ArcadeGame", "Game",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/supertuxkart/stk-code"
homepage = "https://supertuxkart.net"
bugtracker = "https://github.com/supertuxkart/stk-code/issues"
donations = "https://supertuxkart.net/Donate"
translations = "https://supertuxkart.net/Translating_STK"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/supertuxkart/stk-code/master/data/supertuxkart.appdata.xml"
screenshots = [ "https://supertuxkart.net/assets/wiki/STK1.3_1.jpg", "https://supertuxkart.net/assets/wiki/STK1.3_5.jpg", "https://supertuxkart.net/assets/wiki/STK1.3_6.jpg",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "net.supertuxkart.SuperTuxKart"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.supertuxkart.SuperTuxKart"
flatpak_link = "https://flathub.org/apps/net.supertuxkart.SuperTuxKart.flatpakref"
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/supertuxkart"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "supertuxkart",]
appstream_xml_url = "https://raw.githubusercontent.com/supertuxkart/stk-code/master/data/supertuxkart.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++

### Description

Karts. Nitro. Action! SuperTuxKart is a 3D open-source arcade racer with a variety of characters, tracks, and modes to play. Our aim is to create a game that is more fun than realistic, and provide an enjoyable experience for all ages.


We have several tracks with various themes for players to enjoy, from driving underwater, rural farmlands, jungles or even in space! Try your best while avoiding other karts as they may overtake you, but don't eat the bananas! Watch for bowling balls, plungers, bubble gum, and cakes thrown by your opponents.


You can do a single race against other karts, compete in one of several Grand Prix, try to beat the high score in time trials on your own, play battle mode against the computer or your friends, and more! For a greater challenge, join online and meet players from all over the world and prove your racing skills!

[Source](https://raw.githubusercontent.com/supertuxkart/stk-code/master/data/supertuxkart.appdata.xml)