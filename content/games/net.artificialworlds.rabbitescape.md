+++
title = "Rabbit Escape"
description = "Android and PC game inspired by Lemmings and Pingus"
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = ["GPL-2.0-or-later"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["needs testing"]
status = []
frameworks = ["libGDX"]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/andybalaam/rabbit-escape"
homepage = "https://artificialworlds.net/rabbit-escape/"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/andybalaam/rabbit-escape"
screenshots = ["https://artificialworlds.net/rabbit-escape/demo.gif"]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "net.artificialworlds.rabbitescape"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = ["rabbit-escape"]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = ""

+++


