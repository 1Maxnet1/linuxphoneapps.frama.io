+++
title = "Kolorfill"
description = "Color filling game"
aliases = []
date = 2019-02-01
updated = 2024-03-30

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sune Vuorela",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "fedora_rawhide",]
freedesktop_categories = [ "KDE", "Qt", "Game",]
programming_languages = [ "QML", "Cpp", "JavaScript",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://invent.kde.org/games/kolorfill"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = [ "https://pusling.com/blog/?p=505",]
summary_source_url = "https://invent.kde.org/games/kolorfill/-/raw/master/src/org.kde.kolorfill.appdata.xml"
screenshots = [ "https://i.imgur.com/iUCpI9I.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kolorfill"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kolorfill",]
appstream_xml_url = "https://invent.kde.org/games/kolorfill/-/raw/master/src/org.kde.kolorfill.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++


### Description

Kolorfill is a сolor filling game.

[Source](https://invent.kde.org/games/kolorfill/-/raw/master/src/org.kde.kolorfill.appdata.xml)