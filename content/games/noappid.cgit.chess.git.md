+++
title = "GNU Chess"
description = "GNU Chess is a chess-playing program."
aliases = []
date = 2021-03-14
updated = 2023-12-31

[taxonomies]
project_licenses = ["GPL-3.0-or-later"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["5"]
status = []
frameworks = []
backends = []
services = []
packaged_in = ["alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing"]
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://git.savannah.gnu.org/cgit/chess.git"
homepage = "https://www.gnu.org/software/chess/"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://www.gnu.org/software/chess/chess.html"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = ["gnuchess"]
appstream_xml_url = ""
reported_by = "Based_Commgnunism"
updated_by = "script"

+++


### Description

GNU Chess is a chess-playing program. It can be used to play chess against the computer on a terminal or, more commonly, as a chess engine for graphical chess frontends such as Xboard. [Source](https://www.gnu.org/software/chess/chess.html)
