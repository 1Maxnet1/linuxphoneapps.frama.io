+++
title = "Simon Tatham's Portable Puzzle Collection"
description = "A collection of small computer programs which implement one-player puzzle games."
aliases = []
date = 2021-03-24
updated = 2024-03-23

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = []
categories = [ "game", "logic game",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Game", "Logic Game",]
programming_languages = [ "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://git.tartarus.org/?p=simon/puzzles.git"
homepage = "https://www.chiark.greenend.org.uk/~sgtatham/puzzles/"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://www.chiark.greenend.org.uk/~sgtatham/puzzles/"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "sgt-puzzles",]
appstream_xml_url = ""
reported_by = "Moxvallix"
updated_by = "check_via_repology"

+++

### Notice

Games mostly scale well, and can all be played with the touchscreen. Might clog your app draw with 15 different game icons.