+++
title = "Apple Flinger"
description = "Funny single- and multiplayer game for Android - Use a slingshot to shoot with apples. Very challenging and addictive."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = ["GPL-3.0-only"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["needs testing"]
status = []
frameworks = ["libGDX"]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/ar-/apple-flinger"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/ar-/apple-flinger"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.gitlab.ardash.appleflinger"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = ""

+++

