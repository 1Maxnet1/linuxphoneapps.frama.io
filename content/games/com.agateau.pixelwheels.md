+++
title = "Pixel Wheels"
description = "Pixel Wheels is a retro top-down race game."
aliases = [ "games/com.agateau.tinywheels.android/"]
date = 2019-02-16
updated = 2024-03-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = []
categories = [ "game",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "ArcadeGame", "Game",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/agateau/pixelwheels"
homepage = "https://agateau.com/projects/pixelwheels"
bugtracker = "https://github.com/agateau/pixelwheels/issues"
donations = "https://agateau.com/support"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/agateau/pixelwheels/master/tools/packaging/linux/share/metainfo/com.agateau.PixelWheels.metainfo.xml"
screenshots = [ "https://agateau.com/projects/pixelwheels/0.17.0/go.png", "https://agateau.com/projects/pixelwheels/0.17.0/gun.png", "https://agateau.com/projects/pixelwheels/0.17.0/rescued.png", "https://agateau.com/projects/pixelwheels/0.17.0/turbo.png", "https://agateau.com/projects/pixelwheels/0.18.0/a-rocket-for-rocket.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.agateau.PixelWheels"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.agateau.PixelWheels"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pixel-wheels",]
appstream_xml_url = "https://raw.githubusercontent.com/agateau/pixelwheels/master/tools/packaging/linux/share/metainfo/com.agateau.PixelWheels.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"
feed_entry_id = "https://linuxphoneapps.org/games/com.agateau.tinywheels.android/"

+++


### Description

Pixel Wheels is a retro top-down race game for Linux, macOS, Windows and Android.


It features multiple tracks, vehicles. Bonus and weapons can be picked up to help you get to the finish line first!


You can play Pixel Wheels alone or with a friend.

[Source](https://raw.githubusercontent.com/agateau/pixelwheels/master/tools/packaging/linux/share/metainfo/com.agateau.PixelWheels.metainfo.xml)

### Notice

Patched to run by Moxvallix: https://github.com/moxvallix/LinMobGames
