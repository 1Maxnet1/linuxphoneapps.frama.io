+++
title = "Parolottero"
description = "Word game similar to Boggle and Ruzzle"
aliases = []
date = 2023-01-08
updated = 2023-12-31

[taxonomies]
project_licenses = ["AGPL-3.0-only"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["5"]
status = []
frameworks = ["QtQuick"]
backends = []
services = []
packaged_in = ["debian_12", "debian_13", "debian_experimental", "debian_unstable", "devuan_unstable", "pureos_landing"]
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/ltworf/parolottero"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = ["https://www.youtube.com/watch?v=NEwD4Rn_nPQ"]
summary_source_url = "https://github.com/ltworf/parolottero"
screenshots = []
screenshots_img = ["https://img.linuxphoneapps.org/noappid.ltworf.parolottero/1.png", "https://img.linuxphoneapps.org/noappid.ltworf.parolottero/2.png", "https://img.linuxphoneapps.org/noappid.ltworf.parolottero/3.png", "https://img.linuxphoneapps.org/noappid.ltworf.parolottero/4.png"]
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = ["parolottero"]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "script"

+++


### Notice

Language files are at https://github.com/ltworf/parolottero-languages
