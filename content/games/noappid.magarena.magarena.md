+++
title = "Magarena"
description = "Single-player fantasy card game played against a computer opponent."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = ["GPL-3.0-only"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["1"]
status = []
frameworks = []
backends = []
services = []
packaged_in = ["aur"]
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/magarena/magarena"
homepage = "https://magarena.github.io/"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/magarena/magarena"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = ["magarena"]
appstream_xml_url = ""
reported_by = "-Euso-"
updated_by = ""

+++

