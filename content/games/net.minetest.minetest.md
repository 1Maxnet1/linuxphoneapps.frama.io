+++
title = "Minetest"
description = "Multiplayer infinite-world block sandbox game"
aliases = []
date = 2019-02-01
updated = 2024-03-30

[taxonomies]
project_licenses = [ "Apache-2.0", "CC-BY-SA-3.0", "LGPL-2.1-or-later", "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = []
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = []
frameworks = []
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Game", "Simulation",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/minetest/minetest"
homepage = "https://www.minetest.net"
bugtracker = "https://www.minetest.net/development/#reporting-issues"
donations = "https://www.minetest.net/development/#donate"
translations = "https://dev.minetest.net/Translation"
more_information = [ "https://ixit.cz/blog/2023-12-21-Minetest-Linux-touch-pt1",]
summary_source_url = "https://raw.githubusercontent.com/minetest/minetest/master/misc/net.minetest.minetest.appdata.xml"
screenshots = [ "http://www.minetest.net/media/gallery/1.jpg", "http://www.minetest.net/media/gallery/3.jpg", "http://www.minetest.net/media/gallery/5.jpg",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "net.minetest.Minetest"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.minetest.Minetest"
flatpak_link = "https://flathub.org/apps/net.minetest.Minetest.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "minetest",]
appstream_xml_url = "https://raw.githubusercontent.com/minetest/minetest/master/misc/net.minetest.minetest.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++

### Description

Minetest is an infinite-world block sandbox game and game engine.


Players can create and destroy various types of blocks in a
 three-dimensional open world. This allows forming structures in
 every possible creation, on multiplayer servers or in singleplayer.


Minetest is designed to be simple, stable, and portable.
 It is lightweight enough to run on fairly old hardware.


Minetest has many features, including:


* Ability to walk around, dig, and build in a near-infinite voxel world
* Crafting of items from raw materials
* Fast and able to run on old and slow hardware
* A simple modding API that supports many additions and modifications to the game
* Multiplayer support via servers hosted by users
* Beautiful lightning-fast map generator

[Source](https://raw.githubusercontent.com/minetest/minetest/master/misc/net.minetest.minetest.appdata.xml)