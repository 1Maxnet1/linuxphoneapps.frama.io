+++
title = "Ironball"
description = "Mobile game inspired by the classic Speedball game for Amiga/C64 etc."
aliases = []
date = 2019-02-16

[taxonomies]
project_licenses = ["GPL-3.0-only"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["5"]
status = []
frameworks = ["Web technologies"]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/pracedru/ironball"
homepage = "https://www.pracedru.dk:8888/index.html"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/pracedru/ironball/"
screenshots = ["https://github.com/pracedru/ironball/"]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = ""

+++
