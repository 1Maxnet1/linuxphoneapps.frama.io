+++
title = "FreeOrion"
description = "Turn-based space empire and galactic conquest (4X) computer game"
aliases = []
date = 2021-03-14
updated = 2024-03-30

[taxonomies]
project_licenses = [ "CC-BY-SA-3.0", "GPL-2.0-only",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "FreeOrion Project",]
categories = [ "game",]
mobile_compatibility = [ "1",]
status = []
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "arch", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Game", "StrategyGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/freeorion/freeorion"
homepage = "http://www.freeorion.org"
bugtracker = "https://github.com/freeorion/freeorion/issues"
donations = "http://www.freeorion.org/index.php/Donations"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/org.freeorion.FreeOrion"
screenshots = [ "https://www.freeorion.org/screenshots/FreeOrion_GalaxyMapCombatGraph.png", "https://www.freeorion.org/screenshots/FreeOrion_GalaxyMapSystemPlanetsFleets.png", "https://www.freeorion.org/screenshots/FreeOrion_ProductionScreen.png", "https://www.freeorion.org/screenshots/FreeOrion_ResearchScreen.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.freeorion.FreeOrion"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.freeorion.FreeOrion"
flatpak_link = "https://flathub.org/apps/org.freeorion.FreeOrion.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "freeorion",]
appstream_xml_url = "https://raw.githubusercontent.com/freeorion/freeorion/master/packaging/org.freeorion.FreeOrion.metainfo.xml"
reported_by = "-Euso-"
updated_by = "script"

+++

### Description

FreeOrion is a free, Open Source, turn-based space empire and galactic conquest computer game.


FreeOrion is inspired by the tradition of the Master of Orion games, but does not try to be a clone or remake of that series or any other game. It builds on the classic 4X (eXplore, eXpand, eXploit and eXterminate) model.

[Source](https://raw.githubusercontent.com/freeorion/freeorion/master/packaging/org.freeorion.FreeOrion.metainfo.xml)