+++
title = "Blurble"
description = "Word guessing game"
aliases = []
date = 2022-07-16
updated = 2024-03-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Vojtěch Perník",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Game",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Blurble"
homepage = "https://gitlab.gnome.org/World/Blurble"
bugtracker = "https://gitlab.gnome.org/World/Blurble/-/issues"
donations = "https://paypal.me/pervoj"
translations = "https://l10n.gnome.org/module/Blurble"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/Blurble"
screenshots = [ "https://gitlab.gnome.org/World/Blurble/-/raw/v0.4.0/data/screenshots/screenshot-1.png", "https://gitlab.gnome.org/World/Blurble/-/raw/v0.4.0/data/screenshots/screenshot-2.png", "https://gitlab.gnome.org/World/Blurble/-/raw/v0.4.0/data/screenshots/screenshot-3.png", "https://gitlab.gnome.org/World/Blurble/-/raw/v0.4.0/data/screenshots/screenshot-4.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "app.drey.Blurble"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.Blurble"
flatpak_link = "https://flathub.org/apps/app.drey.Blurble.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "blurble",]
appstream_xml_url = "https://gitlab.gnome.org/World/Blurble/-/raw/main/data/blurble.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "script"

+++

### Description

Solve the riddle until you run out of guesses!


The game is a clone of Wordle and made with localization in mind.

[Source](https://gitlab.gnome.org/World/Blurble/-/raw/main/data/blurble.appdata.xml.in.in)