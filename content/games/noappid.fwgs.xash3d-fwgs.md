+++
title = "Xash3D"
description = "Custom Gold Source engine rewritten from scratch."
aliases = []
date = 2021-03-14
updated = 2024-03-09

[taxonomies]
project_licenses = ["not specified"]
metadata_licenses = []
app_author = []
categories = ["game engine"]
mobile_compatibility = ["1"]
status = [ "mature",]
frameworks = ["SDL2"]
backends = []
services = []
packaged_in = ["aur"]
freedesktop_categories = []
programming_languages = [ "C",]
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/FWGS/xash3d-fwgs"
homepage = "https://xash.su/xash3d.html"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/FWGS/xash3d-fwgs"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/FWGS/xash3d-fwgs/master/scripts/flatpak/su.xash.Engine.Compat.i386.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = ["xash3d-fwgs"]
appstream_xml_url = ""
reported_by = "-Euso-"
updated_by = "1peter10"

+++

