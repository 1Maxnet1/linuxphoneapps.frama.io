+++
title = "Caramelos"
description = "Caramelos is an Android Game made with Libgdx"
aliases = []
date = 2019-02-01
updated = 2024-03-09

[taxonomies]
project_licenses = ["MIT"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["needs testing"]
status = [ "inactive",]
frameworks = ["libGDX"]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = [ "Java",]
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/luarca84/Caramelos"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/luarca84/Caramelos"
screenshots = ["https://play.google.com/store/apps/details?id=com.dfzlv.gjjlt.caramelos"]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.dfzlv.gjjlt.caramelos"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1peter10"

+++

### Notice

Last commit 2017-11-08.
