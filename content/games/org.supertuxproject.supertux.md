+++
title = "SuperTux"
description = "A jump-and-run game starring Tux the Penguin"
aliases = []
date = 2019-02-01
updated = 2024-03-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "The SuperTux Team",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "mature",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "snapcraft",]
freedesktop_categories = [ "ActionGame", "ArcadeGame", "Game",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/SuperTux/supertux"
homepage = "https://www.supertux.org"
bugtracker = "https://github.com/SuperTux/supertux/issues"
donations = "https://www.supertux.org/donate.html"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/org.supertuxproject.SuperTux"
screenshots = [ "https://www.supertux.org/images/0_6_0/0_6_0_1.png", "https://www.supertux.org/images/0_6_0/0_6_0_2.png", "https://www.supertux.org/images/0_6_0/0_6_0_3.png", "https://www.supertux.org/images/0_6_0/0_6_0_4.png", "https://www.supertux.org/images/0_6_0/0_6_0_5.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.supertuxproject.SuperTux"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.supertuxproject.SuperTux"
flatpak_link = "https://flathub.org/apps/org.supertuxproject.SuperTux.flatpakref"
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/supertux"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "supertux",]
appstream_xml_url = "https://raw.githubusercontent.com/SuperTux/supertux/master/supertux2.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++

### Description

SuperTux is a jump'n'run game with strong inspiration from the Super Mario Bros. games for the various Nintendo platforms.  Run and jump through multiple worlds, fighting off enemies by jumping on them, bumping them from below or tossing objects at them, grabbing power-ups and other stuff on the way. [Source](https://github.com/SuperTux/supertux)