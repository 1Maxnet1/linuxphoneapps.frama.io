+++
title = "PixelWheels"
description = "Pixel Wheels is a top-down racing game for PC (Linux, Mac, Windows) and Android."
aliases = []
date = 2019-02-16

[taxonomies]
project_licenses = ["GPL-3.0-or-later"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["5"]
status = []
frameworks = ["libGDX"]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/agateau/pixelwheels"
homepage = "https://agateau.com/projects/pixelwheels/"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "http://agateau.com/projects/pixelwheels/"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.agateau.tinywheels.android"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = ""

+++



### Description

Race for the first place on various tracks. Pick up bonuses to boost your position or slow down competitors!" [Source](http://agateau.com/projects/pixelwheels/)


### Notice

Patched to run by Moxvallix: https://github.com/moxvallix/LinMobGames
