+++
title = "1010! Klooni"
description = "libGDX game based on the original 1010!"
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = ["GPL-3.0-or-later"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["5"]
status = []
frameworks = ["libGDX"]
backends = []
services = []
packaged_in = ["aur"]
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/moxvallix/LinMobGames"
homepage = "https://lonami.dev/klooni/"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/LonamiWebs/Klooni1010"
screenshots = ["https://f-droid.org/en/packages/dev.lonami.klooni/"]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "dev.lonami.klooni"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = ["klooni1010"]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = ""

+++


### Notice

Patched to run by Moxvallix.
