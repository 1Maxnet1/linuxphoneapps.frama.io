+++
title = "BurgerParty"
description = "A time management game for Android where you play a fast-food owner who must put together the burgers ordered by her customers before time runs out."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = ["GPL-3.0-or-later"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["needs testing"]
status = []
frameworks = ["libGDX"]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/agateau/burgerparty"
homepage = "https://agateau.com/projects/burgerparty/"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/agateau/burgerparty"
screenshots = ["https://agateau.com/projects/burgerparty/"]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.agateau.burgerparty"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = ""

+++

