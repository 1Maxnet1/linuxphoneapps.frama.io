+++
title = "Zandronum"
description = "Leading the way in newschool multiplayer Doom online"
aliases = []
date = 2021-03-14
updated = 2024-03-30

[taxonomies]
project_licenses = [ "LicenseRef-proprietary",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Zandronum & Doomseeker team",]
categories = [ "game",]
mobile_compatibility = [ "1",]
status = []
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Game", "Shooter",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://osdn.net/projects/zandronum/"
homepage = "https://zandronum.com/"
bugtracker = "https://zandronum.com/tracker/my_view_page.php"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/com.zandronum.Zandronum/master/com.zandronum.Zandronum.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/flathub/com.zandronum.Zandronum/master/images/image_01.png", "https://raw.githubusercontent.com/flathub/com.zandronum.Zandronum/master/images/image_02.png", "https://raw.githubusercontent.com/flathub/com.zandronum.Zandronum/master/images/image_03.png", "https://raw.githubusercontent.com/flathub/com.zandronum.Zandronum/master/images/image_04.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.zandronum.Zandronum"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.zandronum.Zandronum"
flatpak_link = "https://flathub.org/apps/com.zandronum.Zandronum.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "zandronum",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/com.zandronum.Zandronum/master/com.zandronum.Zandronum.appdata.xml"
reported_by = "-Euso-"
updated_by = "script"

+++

### Description

Zandronum is a multiplayer oriented port, based off Skulltag, for Doom and Doom II by id Software. Zandronum brings classic Doom into the 21st century, maintaining the essence of what has made Doom great for so many years and, at the same time, adding new features to modernize it, creating a fresh, fun new experience. Here's why you should kill your time with Zandronum, versus vanilla Doom:


* Client/server architecture: play and switch between games with ease
* Supports up to 64 players
* Wide array of gameplay modes
* Competitive: Team DM, Duel, CTF, Possession, LMS, Terminator, Skulltag
* Cooperative: Survival, Invasion (or both!)
* Modifiers, like Instagib and Buckshot, can be used to spice up any game
* Modern and beautiful
* Support for large number of ZDoom and GZDoom mods
* Choose between GZDoom's OpenGL renderer, or Doom's Software renderer
* Useful features like a console, Quake-style key bindings, freelooking, and jumping
* Support for many Doom engine based games including Heretic, Hexen, and Strife
* Supports modern versions of Windows, Mac OS X, and Linux


In some scenarios, commercial data files are required to run the supported games. In other cases, it's possibly to automatically use Freedoom as a drop-in replacement. This package comes bundled with Doomseeker for easy installation and server browsing.

[Source](https://raw.githubusercontent.com/flathub/com.zandronum.Zandronum/master/com.zandronum.Zandronum.appdata.xml)