+++
title = "OpenRA"
description = "Reimagining of early Westwood real-time strategy games"
aliases = []
date = 2021-03-14
updated = 2024-03-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = []
categories = [ "game",]
mobile_compatibility = [ "1",]
status = []
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "aur", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Game", "StrategyGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/OpenRA/OpenRA"
homepage = "https://www.openra.net"
bugtracker = "https://github.com/OpenRA/OpenRA/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/net.openra.OpenRA"
screenshots = [ "https://www.openra.net/images/appdata/ingame-cnc.png", "https://www.openra.net/images/appdata/ingame-d2k.png", "https://www.openra.net/images/appdata/ingame-ra.png", "https://www.openra.net/images/appdata/multiplayer.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "net.openra.OpenRA"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.openra.OpenRA"
flatpak_link = "https://flathub.org/apps/net.openra.OpenRA.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "openra",]
appstream_xml_url = "https://raw.githubusercontent.com/OpenRA/OpenRA/bleed/packaging/linux/openra.metainfo.xml.in"
reported_by = "-Euso-"
updated_by = "script"

+++