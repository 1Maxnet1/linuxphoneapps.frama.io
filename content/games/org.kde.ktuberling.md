+++
title = "KTuberling"
description = "A simple constructor game suitable for children and adults alike"
aliases = []
date = 2019-02-01
updated = 2024-03-30

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The KTuberling Developers",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = []
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Game", "KidsGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/games/ktuberling"
homepage = "https://apps.kde.org/ktuberling/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=ktuberling"
donations = "https://www.kde.org/community/donations/?app=ktuberling&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/games/ktuberling/-/raw/master/org.kde.ktuberling.appdata.xml"
screenshots = [ "https://dl.flathub.org/media/org/kde/ktuberling.desktop/d204c449b8c650a47ba9be49d2db5e9f/screenshots/image-1_orig.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.ktuberling"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.ktuberling"
flatpak_link = "https://flathub.org/apps/org.kde.ktuberling.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "ktuberling",]
appstream_xml_url = "https://invent.kde.org/games/ktuberling/-/raw/master/org.kde.ktuberling.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++

### Description

KTuberling a simple constructor game suitable for children and adults alike. The idea of the game is based around a once popular doll making concept.

[Source](https://invent.kde.org/games/ktuberling/-/raw/master/org.kde.ktuberling.appdata.xml)