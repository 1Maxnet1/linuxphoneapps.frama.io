+++
title = "H-Craft"
description = "H-Craft Championship is a fun to play scifi-racer."
aliases = []
date = 2019-02-01
updated = 2024-03-09

[taxonomies]
project_licenses = ["z-lib and others", "graphics and fonts are not free licensed!"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["needs testing"]
status = [ "inactive",]
frameworks = ["SDL"]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = [ "C", "Cpp",]
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/mzeilfelder/hc1"
homepage = "http://www.irrgheist.com/games.htm"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "http://www.irrgheist.com/games.htm"
screenshots = ["http://www.irrgheist.com/hcraftscreenshots.htm", "https://play.google.com/store/apps/details?id=com.irrgheist.hcraft_championship",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1peter10"

+++

