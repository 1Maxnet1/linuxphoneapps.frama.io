+++
title = "Arkade"
description = "A collection of games"
aliases = [ "games/org.kde.gamecenter/",]
date = 2020-10-15
updated = 2024-01-02

[taxonomies]
project_licenses = ["GPL-3"]
metadata_licenses = ["CC0-1.0"]
app_author = ["The KDE Community"]
categories = ["game launcher"]
mobile_compatibility = ["5"]
status = []
frameworks = ["Kirigami"]
backends = []
services = []
packaged_in = ["aur", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed"]
freedesktop_categories = ["Game", "KDE", "Qt"]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/games/arkade"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/games/arkade/-/raw/master/org.kde.arkade.appdata.xml"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.arkade"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = ["arkade"]
appstream_xml_url = "https://invent.kde.org/games/arkade/-/raw/master/org.kde.arkade.appdata.xml"
reported_by = "1peter10"
updated_by = "script"
feed_entry_id = "https://linuxphoneapps.org/games/org.kde.gamecenter/"

+++


### Description

Collection of arcade/classic games.

[Source](https://invent.kde.org/games/arkade/-/raw/master/org.kde.arkade.appdata.xml)
