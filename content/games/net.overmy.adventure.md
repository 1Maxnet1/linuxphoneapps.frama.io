+++
title = "Quicky in search of Mickey"
description = "In a beautiful magical forest magic live animals. Among them are our friends Quicky and Mickey. But once Mickey got lost. What happened to him? Quicky will have to find his brother and unravel the mysteries of the magic forest."
aliases = []
date = 2019-03-17

[taxonomies]
project_licenses = ["not specified"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["needs testing"]
status = []
frameworks = ["libGDX"]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/cyberbach/Adventure"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://play.google.com/store/apps/details?id=net.overmy.adventure"
screenshots = ["https://play.google.com/store/apps/details?id=net.overmy.adventure"]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "net.overmy.adventure"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = ""

+++

