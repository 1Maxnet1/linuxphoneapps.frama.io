+++
title = "Unciv"
description = "Turn-based strategy game"
aliases = []
date = 2021-03-17
updated = 2024-03-30

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Yair Morgenstern",]
categories = [ "game",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable",]
freedesktop_categories = [ "Game", "StrategyGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/yairm210/UnCiv"
homepage = "https://github.com/yairm210/Unciv/"
bugtracker = "https://github.com/yairm210/Unciv/issues"
donations = ""
translations = "https://yairm210.github.io/Unciv/Other/Translating/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/io.github.yairm210.unciv/master/io.github.yairm210.unciv.appdata.xml"
screenshots = [ "https://dl.flathub.org/media/io/github/yairm210.unciv/9e78fbd6f6aca55b2ea952e3540092c7/screenshots/image-1_orig.png", "https://dl.flathub.org/media/io/github/yairm210.unciv/9e78fbd6f6aca55b2ea952e3540092c7/screenshots/image-2_orig.png", "https://dl.flathub.org/media/io/github/yairm210.unciv/9e78fbd6f6aca55b2ea952e3540092c7/screenshots/image-3_orig.png", "https://dl.flathub.org/media/io/github/yairm210.unciv/9e78fbd6f6aca55b2ea952e3540092c7/screenshots/image-4_orig.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.yairm210.unciv"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.yairm210.unciv"
flatpak_link = "https://flathub.org/apps/io.github.yairm210.unciv.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "unciv",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/io.github.yairm210.unciv/master/io.github.yairm210.unciv.appdata.xml"
reported_by = "Nob0dy73"
updated_by = "script"

+++

### Description

A reimplementation of the most famous civilization-building game ever—fast, small, no ads, free forever!


Build your civilization, research technologies, expand your cities and defeat your foes!


The world awaits! Will you build your civilization into an empire that will stand the test of time?

[Source](https://raw.githubusercontent.com/flathub/io.github.yairm210.unciv/master/io.github.yairm210.unciv.appdata.xml)

### Notice

The resolution is pretty messed up with the text bordering on unreadable, but otherwise running fine