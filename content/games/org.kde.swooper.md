+++
title = "Swooper"
description = "Minesweeper"
aliases = []
date = 2020-09-28
updated = 2024-01-02

[taxonomies]
project_licenses = ["GPL-3+"]
metadata_licenses = ["FSFAP"]
app_author = ["lui"]
categories = ["game"]
mobile_compatibility = ["5"]
status = []
frameworks = ["Kirigami"]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/luie/Swooper"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/luie/Swooper/-/raw/master/org.kde.swooper.appdata.xml"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.swooper"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/luie/Swooper/-/raw/master/org.kde.swooper.appdata.xml"
reported_by = "1peter10"
updated_by = "script"

+++



### Description

Minesweeper!

[Source](https://invent.kde.org/luie/Swooper/-/raw/master/org.kde.swooper.appdata.xml)