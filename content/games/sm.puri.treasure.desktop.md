+++
title = "Treasure"
description = "An application example, implementing a simple guessing game."
aliases = [ "games/sm.puri.treasure/",]
date = 2019-02-01
updated = 2024-01-02

[taxonomies]
project_licenses = ["GPL-3.0-or-later"]
metadata_licenses = ["CC0-1.0"]
app_author = []
categories = ["game"]
mobile_compatibility = ["needs testing"]
status = []
frameworks = ["GTK3", "libhandy"]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://source.puri.sm/david.boddie/treasure"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/david.boddie/treasure"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "sm.puri.treasure.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://source.puri.sm/david.boddie/treasure/-/raw/master/data/sm.puri.treasure.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
feed_entry_id = "https://linuxphoneapps.org/games/sm.puri.treasure/"

+++
