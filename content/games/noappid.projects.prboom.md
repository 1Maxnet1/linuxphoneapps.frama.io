+++
title = "PrBoom"
description = "PrBoom is the culmination of years of work by various people and projects on the Doom source code."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = ["GPL-2.0-or-later"]
metadata_licenses = []
app_author = []
categories = ["game"]
mobile_compatibility = ["1"]
status = []
frameworks = ["SDL"]
backends = []
services = []
packaged_in = ["aur", "fedora_38", "fedora_39", "fedora_rawhide"]
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://sourceforge.net/projects/prboom/"
homepage = "https://prboom.sourceforge.net/"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://prboom.sourceforge.net/about.html"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = ["prboom"]
appstream_xml_url = ""
reported_by = "-Euso-"
updated_by = ""

+++

