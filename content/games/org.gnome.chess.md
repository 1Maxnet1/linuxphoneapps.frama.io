+++
title = "GNOME Chess"
description = "Play the classic two-player board game of chess"
aliases = []
date = 2021-03-14
updated = 2024-03-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = []
categories = [ "game",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_3_19", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_stable_23_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "BoardGame", "Game",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-chess/"
homepage = "https://wiki.gnome.org/Apps/Chess"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-chess/issues/"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = []
summary_source_url = "https://flathub.org/apps/org.gnome.Chess"
screenshots = [ "https://dl.flathub.org/media/org/gnome/Chess/2459d98addec3261db197b9d04a492e6/screenshots/image-1_orig.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Chess"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Chess"
flatpak_link = "https://flathub.org/apps/org.gnome.Chess.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-chess",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-chess/-/raw/master/data/org.gnome.Chess.appdata.xml.in"
reported_by = "Moxvallix"
updated_by = "script"

+++

### Description

GNOME Chess is a simple chess game. You can play against your computer at
 three different difficulty levels, or against a friend at your computer.


Computer chess enthusiasts will appreciate GNOME Chess’s compatibility with
 nearly all modern computer chess engines, and its ability to detect several
 popular engines automatically if installed.

[Source](https://gitlab.gnome.org/GNOME/gnome-chess/-/raw/master/data/org.gnome.Chess.appdata.xml.in)