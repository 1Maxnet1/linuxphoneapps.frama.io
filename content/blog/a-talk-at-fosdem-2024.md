+++
title = "A Talk at FOSDEM 2024: 'The Linux Phone App Ecosystem'"
date = 2024-02-24T23:57:00+01:00
template = "blog/page.html"
draft = false 
description = "A blog post about FOSDEM 2024's 'The Linux Phone Apps Ecosystem' talk"
[taxonomies]
authors = ["1peter10"]

[extra]
images = []
+++

The what, the how and a lot of addenda to the [The Linux Phone App Ecosystem talk held at FOSDEM 2024](https://fosdem.org/2024/schedule/event/fosdem-2024-3303-the-linux-phone-app-ecosystem/) 

<!-- more -->

### Preface

First things first: FOSDEM (Free Open Source Developers European Meeting) is a yearly event happening in Brussels, Belgium. It lasts only two days, but it has content for a about two weeks. It's a nice event, there's no sign-up, no admission - if you're involved with FOSS, and can make, it should just go. Yes, it's a health hazard and overcrowded. But it's such a boost, also.

After having attended FOSDEM 2023, I was sure to go again. Something made me believe that I should give a talk, and so, as the Call for Participation for the FOSS on Mobile devroom was taking place, I figured that it might be interesting to talk about the Linux Phone App Eosystem - not a bad idea. Also, I figured that my live would contain enough minutes and hours to prepare this well enough. This assumption was wrong, and led to a talk with last minutes slides; thankfully, I had somewhat prepared the rest. But as these things go, without time to rehearse, you will not deliver what you may have intended to deliver.

### The contents

I started out with a few puns and then went on to talk about the main players in the Linux Phone App Ecosystem; first trying to make clear, that Android is not the way to go. 

I then went on a ride through Sailfish OS, Ubuntu Touch and what I called the "new contenders", the GNOME and Plasma Mobile efforts this very website focuesses on. Needless to say, if you want to get into the origins of these projects, and discuss a few highlights from their matters of app distribution, things become challenging in 40 Minute time slot, especially, if you're not a fast talker.

Enough said, maybe.. just watch it?

#### Watch the recording

[FOSDEM 2024 - The Linux Phone App Ecosystem](https://fosdem.org/2024/schedule/event/fosdem-2024-3303-the-linux-phone-app-ecosystem/)

After that, here's where I think I could have done better.

### Areas, where the talk could have been better

Aside from the obvious slide navigations mishaps, and the lack of visual demos, the following should have been said clearer:

#### Sailfish OS

Sailfish is actually, aside from its stores, able to run flatpak apps. However, it's stack (AFAIR it's the wayland compositor) makes it so, that GTK-based apps can't be run. Still, this should make it possible to run Nheko (and thus have a decent enough) Matrix app.

#### Ubuntu Touch

I dropped the ball here, when I did not mention the read-only-filesystem - in a way, Ubuntu Touch is the immutable OG. This is a major characteristic. Also, an image of that donation prompt was sorely missed.

#### New contenders

I am not too unhappy with this, but visuals, again, would have been good. 

Interoperability issues when combining apps from different worlds, starting with what we the keyboard fun when running Qt apps on Phosh (non-automatic) and GNOME Shell Mobile (no keyboard at all), should have been mentioned. 

The foreseeable issues when running Ubuntu Touch apps on any of the new contenders, that unlike Ubuntu Touch use a swipe up gesture for multi tasking, making it unavailable for in app menu things, also (with Sailfish's UI bits being proprietary, a "just build it and run it" approach is impossible, anyway. It will remain challenging and extra work to make an app feel native to each ecosystem, if it's feasible at all. Still, small as all these efforts are, collectively, we should at least try to make this better.

### Where to go from here?

One of my goals with this talk was to name a few examples of apps that essentially work everywhere. I was also touching on the topic of donations and the user appreciation that they can signal in a world of angry bug reports or unfriendly worded feature demands.

Payments though - as I was contacted after the talk, that's likely not worth the hassle, the author of the SailfishOS app Fernschreiber told me, from first hand experience. Transaction costs ruin small payment money transfer - it's a sad fact. Still, if it a mechanism, that can also work for all Linux Desktop users (Flathub is working on this), results may be slightly better if expectations are realistic: This is not iOS, you are unlikely to make a living of one indy app (assuming that's still possible for unknown new-comers over in Apple world, even).

That's the big picture stuff. Personally, I hope to get more in touch with people from both Ubuntu Touch and Sailfish OS, to explore avenues of app interoperability in all directions. Both projects are the most ready for the everyperson, but it would be great if they could participate from the seemingly more energetic space monitored on this website.

Regarding LinuxPhoneApps.org, my 2024 goals include easier maintenance by submitting MRs/PRs to listed projects, so that they can take care of updating listings and are in control of the ways their apps are described (there's a lot more, but this is the most important one, as it can make this project less important, which would be a good thing). This does not mean that I do not intend on making the site better; or that I mean to retire it. New apps showing up are unlikely to be perfect on that metadata; and I don't even know if there are plans to add indicators regarding adaptivity to Discover, Plasma's app management solution.

### Conclusion

Overall, given the circumstances, I am fairly happy with my talk. I hope to publish an enhanced/augmented version of it on TilVids and it's huge competitor in March. If you have any [feedback](https://pretalx.fosdem.org/fosdem-2024/talk/F3PAZY/feedback/), or want to collaborate, do not hesitate to get in touch! ([Public comments are best placed on Mastodon](https://linuxrocks.online/@linuxphoneapps/111989058016448738)).
