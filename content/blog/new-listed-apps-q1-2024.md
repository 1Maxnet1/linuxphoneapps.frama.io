+++
title = "New Listings of Q1/2024: Almost 500 Apps"
date = 2024-04-10T11:30:00+02:00
template = "blog/page.html"
draft = true 

[taxonomies]
authors = ["1peter10"]
+++

Let's have a look at what happened last quarter

<!-- more -->
### New Listings
The following 42 apps and one game were added to LinuxPhoneApps.org in the first quarter of 2024:

#### January
* [Key Rack](https://linuxphoneapps.org/apps/app.drey.keyrack/) - View and edit app secrets 
* [Eloquens](https://linuxphoneapps.org/apps/org.kde.eloquens/) - Generate the lorem ipsum text 
* [Fielding](https://linuxphoneapps.org/apps/org.kde.fielding/) - Test your REST APIs 
* [Journald Browser](https://linuxphoneapps.org/apps/org.kde.kjournaldbrowser.desktop/) - Browser for journald databases 
* [KleverNotes](https://linuxphoneapps.org/apps/org.kde.klevernotes/) - KleverNotes is a note taking and management application 
* [Licentia](https://linuxphoneapps.org/apps/org.kde.licentia/) - Choose a license for your project 
* [MarkNote](https://linuxphoneapps.org/apps/org.kde.marknote/) - Write down your thoughts 
* [Merkuro Mail](https://linuxphoneapps.org/apps/org.kde.merkuro.mail/) - Read your emails with speed and ease 
* [Notae](https://linuxphoneapps.org/apps/org.kde.notae/) - Take notes easily 

#### February
* [Speedtest](https://linuxphoneapps.org/apps/xyz.ketok.speedtest/) - Measure your internet connection speed 
* [fWallet](https://linuxphoneapps.org/apps/business.braid.f_wallet/) - A beautiful cross-platform wallet application for your transport tickets, discount cards and subscriptions. 
* [Electrolysis](https://linuxphoneapps.org/apps/com.gitlab.undef1.electrolysis/) - EV Charging interface for Linux _Thank you, Undef, for adding the app!_
* [LocalSend](https://linuxphoneapps.org/apps/org.localsend.localsend_app/) - Share files to nearby devices 
* [Eeman](https://linuxphoneapps.org/apps/sh.shuriken.eeman/) - Track Salah, Read the Quran 
* [Butler](https://linuxphoneapps.org/apps/com.cassidyjames.butler/) - Companion for Home Assistant 
* [Parabolic](https://linuxphoneapps.org/apps/org.nickvision.tubeconverter/) - Download web video and audio 
* [unlockR](https://linuxphoneapps.org/apps/com.github.jkotra.unlockr/) - PDF Password remover 
* [Muzika](https://linuxphoneapps.org/apps/com.vixalien.muzika/) - Elegant music streaming application 
* [Jupii](https://linuxphoneapps.org/apps/net.mkiol.jupii/) - Play audio, video and images on UPnP/DLNA devices 
* [Whakarere](https://linuxphoneapps.org/apps/com.mudeprolinux.whakarere/) - GTK4 Whatsapp Client 
* [Shortcut](https://linuxphoneapps.org/apps/io.github.andreibachim.shortcut/) - Make app shortcuts 
* [Zodiac](https://linuxphoneapps.org/apps/io.github.alexkdeveloper.zodiac/) - A simple program for plotting horoscopes 
* [Warehouse](https://linuxphoneapps.org/apps/io.github.flattool.warehouse/) - Manage all things Flatpak 
* [Forge Sparks](https://linuxphoneapps.org/apps/com.mardojai.forgesparks/) - Get Git forges notifications 
* [Door Knocker](https://linuxphoneapps.org/apps/xyz.tytanium.doorknocker/) - Check the availability of portals 
* [Chance](https://linuxphoneapps.org/apps/dev.zelikos.rollit/) - Roll the dice 
* [Valuta](https://linuxphoneapps.org/apps/io.github.idevecore.currencyconverter/) - Convert between currencies 
* [Daikhan (Early Access)](https://linuxphoneapps.org/apps/io.gitlab.daikhan.stable/) - Play Videos/Music with style 
* [Time Cop](https://linuxphoneapps.org/apps/ca.hamaluik.timecop/) - A time tracking app that respects your privacy and gets the job done without getting too fancy 
* [Kana](https://linuxphoneapps.org/apps/com.felipekinoshita.kana/) - Learn Japanese characters 
* [Done](https://linuxphoneapps.org/apps/dev.edfloreshz.done/) - To-do lists reimagined 
* [Paper Clip](https://linuxphoneapps.org/apps/io.github.diegoivan.pdf_metadata_editor/) - Edit PDF document metadata 
* [Simple Wireplumber GUI](https://linuxphoneapps.org/apps/io.github.dyegoaurelio.simple-wireplumber-gui/) - A simple GTK4 GUI for PipeWire 
* [Polypass](https://linuxphoneapps.org/apps/io.github.polypixeldev.polypass/) - A simple, secure, and easy to use password manager 
* [Ticket Booth](https://linuxphoneapps.org/apps/me.iepure.ticketbooth/) - Keep track of your favorite shows 
* [GNOME Network Displays](https://linuxphoneapps.org/apps/org.gnome.networkdisplays/) - Screencasting for GNOME 

#### March
* [GoForIt!](https://linuxphoneapps.org/apps/de.manuel_kehl.go-for-it/) - A stylish to-do list with built-in productivity timer 
* [Melon](https://linuxphoneapps.org/apps/icu.ccw.melon/) - Stream videos on the go from different services 
* [Date Calculator](https://linuxphoneapps.org/apps/net.winscloud.datecalculator/) - The go-to minimal date calculator 
* [Memorize](https://linuxphoneapps.org/apps/io.github.david_swift.flashcards/) - Study flashcards 
* [Memorado](https://linuxphoneapps.org/apps/im.bernard.memorado/) - Memorize anything 
* [Folio](https://linuxphoneapps.org/apps/com.toolstack.folio/) - Take notes in Markdown 

#### Games

* [Trivia Quiz](https://linuxphoneapps.org/games/io.github.nokse22.trivia-quiz/) - Respond to endless questions 

### Maintenance

Most listings were automatically updated, and many of them were additionally edited by hand and brought up to date. 
And because information regarding services or frameworks can't be updated automatically ...


## Please contribute

Since April 14th, 2023, the way this site is generated and thus also the ways to contribute to it have been simplified. All relevant parts now live in [one repository](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io).

As a consequence, fixing existing app or game listings is now just one click away (provided you have a framagit account) - every listing has an edit link at the bottom of the page.

![Footer on the bottom of the page](https://linuxphoneapps.org/img/app-list-footer.png)

So if you see that something is wrong, take the time and help fix it!

### Adding apps

If you want to add an app (if you don't know any, check [Apps to be added](https://linuxphoneapps.org/lists/apps-to-be-added/) ;-) ), 

Fortunately, we now have a new and easy way to do this:

 [LPA helper](https://linuxphoneapps.org/lpa_helper.html) is ugly, but it works. We hope to integrate it properly and overhaul its UI soon - if you have questions or suggestions, do not hesitate to ask/get in touch!

See also:
- [Submit apps via GitLab](https://linuxphoneapps.org/docs/contributing/submit-app-on-gitlab/)
- [Listings explained](https://linuxphoneapps.org/docs/contributing/listings-explained/)

You can also send your prepared listing via email to the [devel-list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel).

## What's next?

Stay tuned for a post with our plans and goals for 2024. If you can't wait for that - we do have a [public issue tracker](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues)!

### Feedback, thoughts?

Thank you for reading this post! If you enjoyed it, please spread the word about this post and LinuxPhoneApps.org - it can also be useful for your friends or yourself if you only use the Linux Desktop, as we constantly check for new, awesome adaptive Linux apps!

