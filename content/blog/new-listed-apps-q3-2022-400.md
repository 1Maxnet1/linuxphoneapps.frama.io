+++
title = "New apps of LinuxPhoneApps.org, Q3/2022 - 400! 🎉"
date = 2022-10-08T11:07:00+02:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["1peter10"]

+++

It's October, its time for another quarterly update! Let's get started with some good news: 
LinuxPhoneApps.org now lists over 400 apps, and hit the 400 apps milestone before the end of the quarter. Now how did we get there?
<!-- more -->
### New apps

The following 14 apps where added last quarter:

#### July

* [Pidif](https://linuxphoneapps.org/apps/com.yoavmoshe.pidif/), a lightweight PDF viewer built for touch interfaces, with GTK4 and Rust. _Thanks to bjesus for creating and adding this app!_
* [Quadrix](https://linuxphoneapps.org/apps/chat.quadrix.quadrix/), a minimal, simple, multi-platform chat client for the Matrix protocol (ReactXP, Electron). _Thanks to alpabrz for evaluating and adding this app!_


#### August

* [Lobjur](https://linuxphoneapps.org/apps/com.ranfdev.lobjur/), a GTK4 client for lobste.rs.
* [BadWolf](https://linuxphoneapps.org/apps/me.hacktivis.badwolf/), Minimalist and privacy-oriented web browser based on WebKitGTK. _Thanks lanodan for creating and adding this app!_

#### September

* [Myuzi](https://linuxphoneapps.org/apps/com.gitlab.zehkira.myuzi/). Free Spotify alternative for Linux with no ads. _Thanks to Cara for telling us about this app on Twitter! Please note: This is not a Spotify client!_
* [Syndic](https://linuxphoneapps.org/apps/com.rocksandpaper.syndic/), a convergent feed reader for Plasma and Android.
* [Ear Tag](https://linuxphoneapps.org/apps/app.drey.eartag/),https://github.com/knuxify/eartag,Small and simple music tag editor that doesn't try to manage your entire library,
* [WatchMate](https://linuxphoneapps.org/apps/io.gitlab.azymohliad.watchmate/), a PineTime smart watch companion app for Linux phone and desktop.
* [Gradience](https://linuxphoneapps.org/apps/com.github.gradienceteam.gradience/), an app to change the look of Adwaita, with ease.
* [File Shredder](https://linuxphoneapps.org/apps/com.github.adbeveridge.raider/), a simple shredding program built for the GNOME desktop.
* [PicPlanner](https://linuxphoneapps.org/apps/de.zwarf.picplanner/), an app to calculate the position of the Sun, Moon and Milky Way to plan the position and time for a photograph,
* [GNOME Calendar](https://linuxphoneapps.org/apps/org.gnome.calendar/), Calendar application for GNOME. _With release 43, this app has been redesigned to be mobile friendly. In initial testing (flathub), it misses this goal by a few pixels._ 
* [KeePassPQ](https://linuxphoneapps.org/apps/noappid.hakkropolis.keepasspq/) is a Python and QML based application to open KeePass (v4) databases on mobile devices
and finally
* [Text Pieces](https://linuxphoneapps.org/apps/com.github.liferooter.textpieces/), Transform text without using random websites.


### Maintenance

I also want to take the time to thank everyone who helped with keeping the list up to date via [Merge Request](https://framagit.org/linmobapps/linmobapps.frama.io/-/merge_requests?scope=all&state=merged) or email:

* _alphabrz_ updated Nota, Calindori and Quadrix
* _BigB_ updated Sonically

and _jumpexy_ sent a first mail to our [Errors list](https://lists.sr.ht/~linuxphoneapps/errors) pointing out an error that has since been fixed. 

<mark>!</mark> If you find an error or something outdated in a listing, please scroll down and hit the link after "Something wrong?" at the bottom of the listing. Assuming you have a mail client set up on your system, a new new compose window will open that already has a subject, and some text to make us know what this is about. All you need to do is to describe the error and hit send - it helps a lot! 

Please use this method and not social media to point out errors - social media is just too ephemeral, and your report may be missed (or cause some unneccessary mental load for maintainers ;-) ).

### Help with maintenance!

Overall, maintenance could be better. Sadly my personal life has changed in ways that I (1peter10) have a lot less time to maintain the listings - and at the same time, with more and more listings, there are more projects to watch and maintain. This requires better tooling (e.g., a checker for dead links, or a simple way to follow project releases / commits on "unreleased" projects via RSS/ATOM feeds), that's almost impossible to build for me in the few scraps of time I have in an average work week.

Sadly, due to spammers, framagit has enabled account moderation - signing up for a new account is not instantaneous and may take days. But you can of still contribute quickly: Just clone the repo, create a patch and send it to our [devel mailing list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel)! 

### The way forward

#### Now: More ways to contribute and communicate

As mentioned above, we've added another way to contribute. Signing up for yet another code forge is something you may not want, and you don't have to. While you could also send patches via email before, there's now an official way for that documented on the [devel mailing list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel).

If you've developed an app (or found an app that's not listed) and don't want to bother with the arcane csv file, feel free to write about it to our [discuss mailing list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss). 

#### Replacing CSV? TOML? metainfo.xml?

Stay tuned for a post later this quarter that will contain my ideas to move beyond the current way which is somewhat convoluted - yes, I wrote "ideas" here, because I need the input of more savvy people to come up with an actual plan. The goal is to form a plan in 2022 and to then implement it in early 2023.

### Also: Hacktoberfest

Lastly, we briefly want to mention [hacktoberfest](https://hacktoberfest.com/).[^1] While contributing LinuxPhoneApps.org won't help you get a t-shirt, there are a few listed Linux Mobile projects that are accepting contributions during hacktoberfest:

* [Axolotl](https://github.com/nanu-c/axolotl),
* [Cawbird](https://github.com/IBBoard/cawbird),
* [Gradience](https://github.com/GradienceTeam/Gradience),
* [Myuzi](https://gitlab.com/zehkira/myuzi), and
* [Nheko](https://github.com/Nheko-Reborn/nheko)

(Thanks to [@JJS_10 for helping with compiling this list](https://mastodon.online/web/@JJS_10/109112281121899562)!)


### Feedback, thoughts?

Thank you for reading this post! If you enjoyed it, please spread the word about this post and LinuxPhoneApps.org!

If you want to contribute an/your app or work on the website itself, please check the [FAQ](@/docs/help/faq.md#join-the-effort)![^2] 

And if you want to discuss apps with like minded persons, make sure to join [our Matrix group](https://matrix.to/#/#linuxphoneapps:matrix.org) or subscribe/post to our [discuss mailing list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss)!


[^1]: Hacktoberfest has a bad reputation from leading to spammy commits, but after reading [their guidelines for this year](https://hacktoberfest.com/participation/), I think they have addressed this.

[^2]: If you are not aware of any new apps, check out [apps-to-be-added](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/apps-to-be-added.md), test the app and add it!
